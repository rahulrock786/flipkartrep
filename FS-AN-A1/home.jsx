import React, { Component } from "react";
import * as flipkartService from "../../services/flipkartService";
import FlipkartNavbar from "./header";

import Banner from "./banner";
import { paginate } from "./../utils/paginate";
import { connect } from "react-redux";
import queryString from "query-string";
import { getCurrentUser } from "./../../services/flipkartService";
import { postUrl } from "../common/apiCall";

class Home extends Component {
  state = { products: [], pageSize: 5, currentPage: 1 };
  async componentDidMount() {
    try {
      const response = await flipkartService.getAllDeals();
      const { data } = response;
      this.setState({ products: data });
    } catch (error) {}
  }
  handlePage = (x) => {
    this.setState({ currentPage: this.state.currentPage + x });
  };
  handleDealProd = async (item) => {
    let { category, brand, id } = item;
    postUrl(`/home/product/${category}/${brand}/${id}`);
    this.props.history.push(`/home/product/${category}/${brand}/${id}`);
  };

  handleBannerProd = async () => {
    postUrl(`/home/product/Mobiles/RealMe/M17`);
    this.props.history.push("/home/product/Mobiles/RealMe/M17");
  };
  handleSearch = async (search) => {
    let { ram, rating, price, sort, assured } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    params = this.addToParams(params, "q", search);
    params = this.addToParams(params, "page", 1);
    if (ram) params = this.addToParams(params, "ram", ram);
    if (rating) params = this.addToParams(params, "rating", rating);
    if (price) params = this.addToParams(params, "price", price);
    if (sort) params = this.addToParams(params, "sort", sort);
    if (assured) params = this.addToParams(params, "assured", assured);
    let data = {
      keyword: search,
    };
    try {
      let response = await flipkartService.postSearchReport(data);
      if (response) {
        postUrl(`/home/products${params}`);
        this.props.history.push(`/home/products${params}`);
      }
    } catch (error) {}
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamName) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handlePic = () => {
    postUrl(`/home/product/Mobiles/RealMe/M20`);
    this.props.history.push("/home/product/Mobiles/RealMe/M20");
  };
  render() {
    const { products, currentPage, pageSize } = this.state;
    const items = paginate(products, currentPage, pageSize);

    return (
      <div className="container-fluid" style={{ backgroundColor: "#d4d3cf" }}>
        <FlipkartNavbar onSubmit={this.handleSearch} />
        <Banner onCart={this.handleBannerProd} />
        <div className="row mt-1">
          <div
            className="col-lg-9 col-12 border border-bottom  ml-0 border"
            style={{ backgroundColor: "white", fontSize: 22 }}
          >
            <b>Deals of the Day</b>
            <hr />
            <div className="row">
              {currentPage > 1 ? (
                <div className="col-1">
                  <button
                    style={{
                      alignSelf: "center",
                      padding: "60px 5px",
                      color: "black",
                      boxShadow: "1px 2px 10px -1px rgba(0,0,0,.3)",
                      backgroundColor: "hsla(0,0%,100%,.98)",
                      cursor: "pointer",
                      display: "flex",
                      position: "absolute",
                      zIndex: 1,
                    }}
                    onClick={() => this.handlePage(-1)}
                  >
                    <i className="fa fa-angle-left"></i>
                  </button>
                </div>
              ) : (
                ""
              )}

              {items && items.length > 0
                ? items.map((item, index) => (
                    <div className="col-lg-2 col-3   " key={index}>
                      <div className="row   text-center">
                        <div className="col-12">
                          <img
                            style={{ height: 150, cursor: "pointer" }}
                            src={item.img}
                            onClick={() => this.handleDealProd(item)}
                          />
                        </div>
                      </div>
                      <div
                        className="row p-0 "
                        style={{ fontSize: 12, fontWeight: 500, marginTop: 15 }}
                      >
                        <div className="col-12 text-truncate text-center">
                          {item.name}
                        </div>
                      </div>
                      <div className="row text-right" id="abc">
                        <div
                          className="col"
                          id="abc"
                          style={{ color: "#388e3c", fontSize: 10 }}
                        >
                          {item.discount} % Off
                        </div>
                      </div>
                    </div>
                  ))
                : ""}
              {pageSize === items.length ? (
                <div className="col-1 text-right  ">
                  <button
                    style={{
                      alignSelf: "center",
                      padding: "60px 5px",
                      color: "black",
                      boxShadow: "1px 2px 10px -1px rgba(0,0,0,.3)",
                      backgroundColor: "hsla(0,0%,100%,.98)",
                      cursor: "pointer",
                      display: "flex",
                      position: "absolute",
                      marginLeft: 45,
                      zIndex: 1,
                    }}
                    onClick={() => this.handlePage(1)}
                  >
                    <i className="fa fa-angle-right"></i>
                  </button>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="col-2 border-light ml-1 bg-white text-right d-none d-lg-block">
            <img
              className="img-fluid"
              src="https://i.ibb.co/1GBrRnn/fa04c5362949d9f1.jpg"
              style={{ width: "auto" }}
            />
          </div>
        </div>
        <div className="row ml-1 mt-1">
          <div className="col-4  text-center">
            <img
              className="img-fluid"
              src="https://i.ibb.co/dPVHZGW/d5db30a716f82657.jpg"
              style={{ width: "auto", height: "auto" }}
              alt=""
              onClick={() => this.handlePic()}
            />
          </div>
          <div className="col-4   text-center">
            <img
              className="img-fluid"
              src="https://i.ibb.co/Lzz36zB/31efaad41a3e4208.jpg"
              style={{ width: "auto" }}
              alt=""
            />
          </div>
          <div className="col-4  text-center">
            <img
              className="img-fluid"
              src="https://i.ibb.co/fGX7sFh/4e219998fadcbc70.jpg"
              style={{ width: "auto" }}
              alt=""
            />
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Home);
