import React, { Component } from "react";
import { getReport } from "../../../../services/flipkartService";
import { CSVLink } from "react-csv";
import { ExportToCsv } from "export-to-csv";
import EditProduct from "../editProduct";
class UsageReport extends Component {
  state = {
    reports: [
      { name: "Most Added Product in Cart Report", apiend: "cartReport" },
      { name: "Most Search Product Report", apiend: "searchReport" },
      { name: "Most Add as Favourite Report", apiend: "favReport" },
      { name: "User Activity Report", apiend: "userActivityReport" },
      { name: "All Products Details", apiend: "allProductReport" },
    ],
    reportSel: "",
    data: [],
    editId: null,
  };

  downloadReport = async (apiEnd) => {
    const options = {
      fieldSeparator: ",",
      quoteStrings: '"',
      decimalSeparator: ".",
      showLabels: true,
      showTitle: false,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    try {
      let response = await getReport(apiEnd);
      csvExporter.generateCsv(response.data);
    } catch (error) {}
  };
  viewReport = async (apiEnd) => {
    this.setState({ reportSel: apiEnd });
    try {
      let response = await getReport(apiEnd);
      this.setState({ data: response.data, editId: null });
    } catch (error) {}
  };

  handleEdit = (id) => {
    this.setState({ editId: id });
  };
  render() {
    const { reports, data, reportSel, editId } = this.state;
    return (
      <>
        {reports.map((report) => (
          <div className="row mx-1 " id="Usage Report">
            <div className="col-7 col-sm-9 col-md-10 py-2 border border-dark bg-primary text-white my-1">
              <span
                style={{
                  fontSize: "clamp(12px,2vw,20px)",
                  fontFamily: "Serif",
                }}
              >
                {report.name}
              </span>
            </div>
            <div className="col border p-0 my-1">
              <button
                className="btn btn-warning w-100 text-center"
                style={{
                  height: "100%",
                  fontSize: "clamp(8px,2vw,14px)",
                  fontWeight: "bold",
                  borderRadius: 0,
                }}
                onClick={() => this.viewReport(report.apiend)}
              >
                View
              </button>
            </div>
            <div className="col border p-0 my-1">
              <button
                className="btn btn-success w-100"
                style={{
                  height: "100%",
                  fontSize: "clamp(8px,2vw,14px)",
                  fontWeight: "bold",
                  borderRadius: 0,
                }}
                onClick={() => this.downloadReport(report.apiend)}
              >
                Download
              </button>
            </div>
          </div>
        ))}

        {data && data.length > 0 && editId === null ? (
          <>
            <div
              className="row mx-1 mt-2 text-center bg-primary text-white mb-1"
              style={{
                fontSize: "clamp(10px,2vw,20px)",
                fontFamily: "Serif",
              }}
            >
              <div className="col-md-1  d-md-block d-none">S.NO</div>
              {reportSel === "searchReport" ? (
                <>
                  <div className="col-2 col-sm-2 border">KeyWord</div>
                  <div className="col border ">Total Volume Count</div>
                </>
              ) : (
                ""
              )}
              {reportSel === "userActivityReport" ? (
                <>
                  <div className="col-2 col-sm-3 border">Id</div>
                  <div className="col border ">Url</div>
                  <div className="col border ">Token</div>
                </>
              ) : (
                ""
              )}
              {reportSel === "favReport" ? (
                <>
                  <div className="col-2 col-sm-2 border">Product Id</div>
                  <div className="col border">Product Name</div>
                  <div className="col border ">Product Image</div>
                  <div className="col border ">Total Volume Count</div>
                  <div className="col border d-block text-right p-0 "></div>
                </>
              ) : (
                ""
              )}

              {reportSel === "cartReport" ? (
                <>
                  <div className="col-2 col-sm-2 border">Product Id</div>
                  <div className="col border">Product Name</div>
                  <div className="col border ">Product Image</div>
                  <div className="col border ">Total Volume Count</div>
                  <div className="col border d-block text-right p-0 "></div>
                </>
              ) : (
                ""
              )}
              {reportSel === "allProductReport" ? (
                <>
                  <div className="col-2 col-sm-2 border">Product Id</div>
                  <div className="col border">Product Name</div>
                  <div className="col border ">Product Image</div>
                  <div className="col border d-block text-right p-0 "></div>
                </>
              ) : (
                ""
              )}
            </div>

            {data.map((obj, index) => (
              <div
                className="row mx-1 text-center"
                style={{
                  fontSize: "clamp(8px,2vw,16px)",
                  fontFamily: "Serif",
                }}
              >
                <div className="col-md-1 border d-md-block d-none">
                  {index + 1}
                </div>

                {reportSel === "searchReport" ? (
                  <>
                    <div className="col-2 col-sm-2 border">{obj.keyword}</div>
                    <div className="col border ">{obj.volume}</div>
                  </>
                ) : (
                  ""
                )}
                {reportSel === "userActivityReport" ? (
                  <>
                    <div className="col-2 col-sm-3 border text-truncate">
                      {obj.id}
                    </div>
                    <div className="col border text-truncate ">{obj.url}</div>
                    <div className="col border text-truncate">{obj.token}</div>
                  </>
                ) : (
                  ""
                )}
                {reportSel === "favReport" ? (
                  <>
                    <div className="col-2 col-sm-2 border">{obj.id}</div>
                    <div className="col border">{obj.name}</div>
                    <div className="col border ">
                      <img src={obj.img} style={{ width: 50, height: 80 }} />
                    </div>
                    <div className="col border ">{obj.volume}</div>
                    <div
                      className="col border d-block text-right p-0"
                      style={{ verticalAlign: "center" }}
                    >
                      <button
                        className="btn btn-warning w-100 "
                        style={{
                          overflow: "hidden",
                          height: "100%",
                          borderRadius: 0,
                        }}
                        onClick={() => this.handleEdit(obj.id)}
                      >
                        <i className="fa fa-edit"></i> &nbsp;Edit
                      </button>
                    </div>
                  </>
                ) : (
                  ""
                )}

                {reportSel === "cartReport" ? (
                  <>
                    <div className="col-2 col-sm-2 border">{obj.id}</div>
                    <div className="col border">{obj.name}</div>
                    <div className="col border ">
                      {" "}
                      <img src={obj.img} style={{ width: 50, height: 80 }} />
                    </div>
                    <div className="col border ">{obj.volume}</div>
                    <div
                      className="col border d-block text-right p-0"
                      style={{ verticalAlign: "center" }}
                    >
                      <button
                        className="btn btn-warning w-100 "
                        style={{
                          overflow: "hidden",
                          height: "100%",
                          borderRadius: 0,
                        }}
                        onClick={() => this.handleEdit(obj.id)}
                      >
                        <i className="fa fa-edit"></i> &nbsp;Edit
                      </button>
                    </div>
                  </>
                ) : (
                  ""
                )}
                {reportSel === "allProductReport" ? (
                  <>
                    <div className="col-2 col-sm-2 border">{obj.id}</div>
                    <div className="col border">{obj.name}</div>
                    <div className="col border ">
                      <img src={obj.img} style={{ width: 50, height: 80 }} />
                    </div>
                    <div
                      className="col border d-block text-right p-0"
                      style={{ verticalAlign: "center" }}
                    >
                      <button
                        className="btn btn-warning w-100 "
                        style={{
                          overflow: "hidden",
                          height: "100%",
                          borderRadius: 0,
                        }}
                        onClick={() => this.handleEdit(obj.id)}
                      >
                        <i className="fa fa-edit"></i> &nbsp;Edit
                      </button>
                    </div>
                  </>
                ) : (
                  ""
                )}
              </div>
            ))}
          </>
        ) : (
          ""
        )}

        {editId !== null ? (
          <div className="row">
            <div className="col-12">
              <EditProduct id={editId} />
            </div>
          </div>
        ) : (
          ""
        )}
      </>
    );
  }
}

export default UsageReport;
