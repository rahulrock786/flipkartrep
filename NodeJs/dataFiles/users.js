const users = [
  {
    id: 1,
    name: "Rahul",
    email: "rahul@gmail.com",
    password: "123456",
    role: "admin",
  },
  {
    id: 2,
    name: "Pankaj",
    email: "pankaj@gmail.com",
    password: "123456",
    role: "user",
  },
];

module.exports.users = users;
