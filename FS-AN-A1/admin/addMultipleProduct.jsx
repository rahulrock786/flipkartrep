import { Field, Form, Formik } from "formik";
import React, { Component } from "react";
import { Link } from "react-scroll";

import { addMultipleProduct } from "../../../services/flipkartService";

class AddMultipleProduct extends Component {
  state = { file: "", fileName: "Choose File", results: [] };

  handleChange = (e) => {
    let { currentTarget: input } = e;
    console.log(e.target.files);
    this.setState({
      file: e.target.files[0],
      fileName: e.target.files[0].name,
    });
  };

  handleSubmit = async (e) => {
    let file = this.state.file;
    e.preventDefault();
    if (file === "") {
      alert("No file Selected");
      return;
    }
    let formData = new FormData();
    formData.append("myFile", this.state.file, this.state.fileName);
    console.log(formData);
    try {
      let response = await addMultipleProduct(formData);
      console.log(response);
      this.setState({ results: response.data });
    } catch (error) {
      if (error) console.log(error);
      alert("Not a Valid Csv File Please Upload Again !");
      this.setState({ results: [] });
    }
  };

  render() {
    const { results } = this.state;
    return (
      <>
        <div className="row border mt-2 p-3">
          <div className="col-12 text-center">
            <form onSubmit={this.handleSubmit}>
              <div className="custom-file">
                <input
                  type="file"
                  className="custom-file-input"
                  id="customFile"
                  name="samplefile"
                  onChange={this.handleChange}
                />
                <label className="custom-file-label" htmlFor="customFile">
                  {this.state.fileName}
                </label>
                <input
                  type="submit"
                  className="btn btn-primary my-2"
                  value="Upload"
                />
              </div>
            </form>
          </div>
        </div>

        {results && results.length > 0 ? (
          <div className="row" style={{ height: 550, overflowY: "scroll" }}>
            <div className="col-12">
              <div className="row mt-2 py-1 border-dark  text-center">
                <div className="col border h5 p-1">S.No</div>
                <div className="col border h5 p-1">Product Id</div>
                <div className="col border h5 p-1">Product Category</div>
                <div className="col border h5 p-1">Status</div>
                <div className="col border h5 p-1">Reason</div>
              </div>

              {results.map((result, index) => (
                <div className="row my-1 text-center">
                  <div className="col border border-grey bg-light shadow p-1">
                    {index + 1}
                  </div>
                  <div className="col border border-grey bg-light shadow p-1">
                    {result.productId}
                  </div>
                  <div className="col border border-grey bg-light shadow p-1">
                    {result.category}
                  </div>
                  {result.statusCode === 200 ? (
                    <div className="col border border-grey bg-light shadow p-1 text-grey bg-light">
                      {result.status}
                    </div>
                  ) : (
                    <div className="col border border-grey bg-light shadow p-1 text-danger">
                      {result.status}
                    </div>
                  )}

                  <div className="col border border-grey bg-light shadow p-1">
                    {result.reason}
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          ""
        )}
      </>
    );
  }
}

export default AddMultipleProduct;
