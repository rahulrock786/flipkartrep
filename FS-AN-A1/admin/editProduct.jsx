import { Field, FieldArray, Form, Formik } from "formik";
import React, { Component } from "react";
import FormInput from "../../common/input";
import "./addSingleProduct.css";
import {
  editExistingProduct,
  getProductById,
} from "./../../../services/flipkartService";

class EditProduct extends Component {
  state = {
    categories: ["Mobiles", "Laptops", "Cameras"],
    formData: null,
  };
  styles = {
    label: {
      fontSize: "clamp(16px,2vw,22px)",
      fontWeight: "bold",
      fontFamily: "sans-serif",
    },
    product: {
      fontSize: "clamp(16px,2vw,25px)",
      fontWeight: "bold",
    },
  };

  constructor(props) {
    super(props);
    this.state.loaded = false;
  }

  async componentDidMount() {
    let id = this.props.id;
    try {
      let response = await getProductById("", id);
      console.log(response);
      let formData = {
        ...response.data.prod,
        ...response.data.details,
        ...response.data.pics,
      };
      this.setState({ formData, loaded: true });
    } catch (error) {}
  }

  handleSubmit = async (values, { ...rest }) => {
    let data = {
      prod: {
        id: values.id,
        category: values.category,
        brand: values.brand,
        name: values.name,
        emi: values.emi,
        rating: values.rating,
        review: values.review,
        details: values.details,
        price: values.price,
        prevPrice: values.prevPrice,
        discount: values.discount,
        img: values.img,
        assured: values.assured === "yes" ? true : false,
        exchange: values.exhange,
        ram: values.ram,
        popularity: values.popularity,
      },
      pics: {
        brand: values.brand,
        imgList: values.imgList,
        brandImg: values.brandImg,
      },
    };

    try {
      let response = await editExistingProduct(values.id, data);
      if (response) {
        alert(response.data);
      }
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    const { label } = this.styles;
    const { categories, formData, loaded } = this.state;

    return loaded ? (
      <Formik initialValues={formData} onSubmit={this.handleSubmit}>
        {({ values, handleChange, errors }) => {
          return (
            <Form>
              <>
                <div className="row border my-3 py-3  ">
                  <div className="col-12">
                    <div className="row my-2 bg-light mx-1 border pt-2 ">
                      <div className="col-12 col-md">
                        <FormInput
                          type="text"
                          value={values.id}
                          label="Product-Id"
                          name="id"
                          disabled={true}
                          onChange={handleChange}
                        />
                      </div>
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-12">
                            <FormInput
                              type="select"
                              value={values.category}
                              label="Select Product Category"
                              name="category"
                              options={categories}
                              onChange={handleChange}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <FormInput
                          type="text"
                          value={values.brand}
                          label="Brand Name"
                          name="brand"
                          onChange={handleChange}
                        />
                      </div>
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-12">
                            <FormInput
                              type="text"
                              value={values.name}
                              label="Product Name"
                              name="name"
                              onChange={handleChange}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <FormInput
                          type="text"
                          value={values.img}
                          label="Set Product Picture"
                          name="img"
                          onChange={handleChange}
                        />
                      </div>
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-12">
                            <FormInput
                              type="text"
                              value={values.rating}
                              label="Give Rating of Your Product"
                              name="rating"
                              onChange={handleChange}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <FormInput
                          type="number"
                          value={values.review}
                          label="Total Review"
                          name="review"
                          onChange={handleChange}
                        />
                      </div>
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-12">
                            <FormInput
                              type="text"
                              value={values.ratingDesc}
                              label="Rating Desc"
                              name="ratingDesc"
                              onChange={handleChange}
                              placeholder="Give Rating Description"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <FieldArray name="details">
                      {(fieldArrayProps) => {
                        const { push, remove, form, name } = fieldArrayProps;

                        const details = values.details;
                        return (
                          <div className="row border mx-1 py-1 bg-light mt-5">
                            <div className="col-12">
                              <div className="row">
                                <div className="col-9 col-sm-10 pt-2 pl-3">
                                  <span
                                    style={{
                                      fontSize: "clamp(16px,2vw,18px)",
                                      fontWeight: 550,
                                      fontFamily: "Arial",
                                      color: "#666666",
                                    }}
                                  >
                                    Add Product Details
                                  </span>
                                </div>
                                <div className="col col-sm text-center">
                                  <button
                                    type="button"
                                    className="btn btn-outline-primary my-2"
                                    onClick={() => push("")}
                                    style={{
                                      fontWeight: "450px",
                                      width: "100%",
                                    }}
                                  >
                                    Add
                                  </button>
                                  {/* {errors.details ? (
                                <div className="text-danger">
                                  {errors.details}
                                </div>
                              ) : null} */}
                                </div>
                              </div>

                              {details.map((item, index) => (
                                <div className="row">
                                  <div className="col-9 col-sm-10 my-3 pr-0">
                                    <Field
                                      className="form-control"
                                      name={`details[${index}]`}
                                      placeholder="Item Name"
                                      style={{ width: "100%" }}
                                    />
                                  </div>

                                  <div className="col col-sm my-3 text-left d-sm-block d-none">
                                    <button
                                      type="button"
                                      className="btn btn-primary mr-1"
                                      style={{ width: "100%" }}
                                      onClick={() => remove(index)}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                  <div className="col col-sm my-3 text-left d-sm-none">
                                    <button
                                      type="button"
                                      className="btn btn-primary mr-1"
                                      style={{ width: "100%" }}
                                      onClick={() => remove(index)}
                                    >
                                      -
                                    </button>
                                  </div>
                                </div>
                              ))}
                            </div>
                          </div>
                        );
                      }}
                    </FieldArray>
                    <FieldArray name="imgList">
                      {(fieldArrayProps) => {
                        const { push, remove, form, name } = fieldArrayProps;

                        const imgList = values.imgList;
                        return (
                          <div className="row border mx-1 py-2 bg-light mt-5">
                            <div
                              className="col-12 col-sm-8"
                              style={{ height: "400px", overflowY: "scroll" }}
                            >
                              <div
                                className="row"
                                style={{ position: "sticky" }}
                              >
                                <div className="col-9 col-sm-10 pt-2 pl-3">
                                  <span
                                    style={{
                                      fontSize: "clamp(16px,2vw,18px)",
                                      fontWeight: 550,
                                      fontFamily: "Arial",
                                      color: "#666666",
                                    }}
                                  >
                                    Add Product Images
                                  </span>
                                </div>
                                <div className="col col-sm text-center d-sm-block d-none">
                                  <button
                                    type="button"
                                    className="btn btn-outline-primary my-2"
                                    onClick={() => push("")}
                                    style={{
                                      fontWeight: "450px",
                                      width: "100%",
                                    }}
                                  >
                                    Add
                                  </button>

                                  {/* {errors.details ? (
                                <div className="text-danger">
                                  {errors.details}
                                </div>
                              ) : null} */}
                                </div>
                                <div className="col col-sm text-center d-sm-none">
                                  <button
                                    type="button"
                                    className="btn btn-outline-primary my-2  text-center"
                                    onClick={() => push("")}
                                    style={{
                                      fontWeight: "450px",
                                      width: "100%",
                                    }}
                                  >
                                    +
                                  </button>

                                  {/* {errors.details ? (
                                <div className="text-danger">
                                  {errors.details}
                                </div>
                              ) : null} */}
                                </div>
                              </div>

                              {imgList.map((item, index) => (
                                <div className="row">
                                  <div className="col-9 col-sm-10 my-3 pr-0">
                                    <Field
                                      className="form-control"
                                      name={`imgList[${index}]`}
                                      placeholder="Item Name"
                                      style={{ width: "100%" }}
                                      onChange={handleChange}
                                    />
                                  </div>

                                  <div className="col col-sm my-3 text-left d-sm-block d-none">
                                    <button
                                      type="button"
                                      className="btn btn-primary mr-1"
                                      style={{ width: "100%" }}
                                      onClick={() => remove(index)}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                  <div className="col col-sm my-3 text-left d-sm-none">
                                    <button
                                      type="button"
                                      className="btn btn-primary mr-1"
                                      style={{ width: "100%" }}
                                      onClick={() => remove(index)}
                                    >
                                      -
                                    </button>
                                  </div>
                                </div>
                              ))}
                            </div>
                            <div
                              className="col-12 col-sm d-flex py-2 text-center"
                              style={{
                                flexWrap: "wrap",
                                height: "400px",
                                overflowY: "scroll",
                              }}
                            >
                              <div>
                                {values.imgList.map((img, index) => (
                                  <img
                                    className="border border-dark m-1"
                                    src={
                                      img
                                        ? img
                                        : "https://i.ibb.co/HgxPWsp/img1.png"
                                    }
                                    alt={index}
                                    style={{ width: 80, height: 80 }}
                                  />
                                ))}
                              </div>
                            </div>
                          </div>
                        );
                      }}
                    </FieldArray>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-6">
                            <FormInput
                              type="text"
                              name="brandImg"
                              value={values.brandImg}
                              onChange={handleChange}
                              label="Brand Image"
                            />
                          </div>
                          <div className="col-6">
                            <div></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col">
                            <FormInput
                              type="number"
                              name="price"
                              value={values.price}
                              onChange={handleChange}
                              placeholder="Rs."
                              label="Set Maximum Retail Price"
                            />
                          </div>
                          <div className="col">
                            <FormInput
                              type="number"
                              name="prevPrice"
                              value={values.prevPrice}
                              onChange={handleChange}
                              placeholder="Rs."
                              label="Maximum Price Before Discount"
                            />
                          </div>
                          <div className="col">
                            <FormInput
                              type="number"
                              name="discount"
                              value={values.discount}
                              onChange={handleChange}
                              placeholder="%"
                              label="Set Discount"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-6">
                            <FormInput
                              type="number"
                              name="exchange"
                              value={values.exhange}
                              onChange={handleChange}
                              placeholder="Rs."
                              label="Set Exchange Price"
                            />
                          </div>
                          <div className="col-6">
                            <FormInput
                              type="number"
                              name="popularity"
                              value={values.popularity}
                              onChange={handleChange}
                              placeholder="%"
                              label="Popularity"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-6 text-truncate">
                            <FormInput
                              type="number"
                              name="ram"
                              value={values.ram}
                              onChange={handleChange}
                              placeholder="GB"
                              label="Set Ram (Random Access Memory)"
                            />
                          </div>
                          <div className="col-6 text-truncate">
                            <FormInput
                              type="number"
                              name="ROM"
                              value={values.ROM}
                              onChange={handleChange}
                              placeholder="GB"
                              label="ROM (Read Only Memory)"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light mx-1 border pt-2">
                      <div className="col-12 col-md">
                        <div className="row">
                          <div className="col-6">
                            <FormInput
                              type="number"
                              name="effectivePixel"
                              value={values.effectivePixe}
                              onChange={handleChange}
                              placeholder="Pixel"
                              label="Camera Pixel"
                            />
                          </div>
                          <div className="col-6">
                            <FormInput
                              type="text"
                              name="assured"
                              value={values.assured}
                              onChange={handleChange}
                              placeholder="Yes or No"
                              label="Assured"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row my-2 bg-light my-1  pt-2">
                      <div className="col-6"></div>
                      <div className="col-6 text-left">
                        <button
                          type="submit"
                          className="btn btn-primary btn-lg"
                        >
                          Submit Product{" "}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            </Form>
          );
        }}
      </Formik>
    ) : (
      ""
    );
  }
}

export default EditProduct;
