import React, { Component } from "react";
import { Carousel } from "react-responsive-carousel";

import "react-responsive-carousel/lib/styles/carousel.min.css";

class Banner extends Component {
  state = {
    urls: [
      " https://i.ibb.co/4m9zL2Y/aa8947d0a8f758f2.jpg",
      " https://i.ibb.co/tq9j6V7/4dfdf0c59f26c4a1.jpg",
      " https://i.ibb.co/vQZhcvT/68af1ae7331acd1c.jpg",
      " https://i.ibb.co/qxHzVsp/30d7dffe1a1eae09.jpg",
    ],
  };
  img = {
    width: "100%",
    maxHeight: 310,
    minHeight: 100,
    cursor: "pointer",
  };

  render() {
    const { urls } = this.state;
    return (
      <Carousel
        showThumbs={false}
        showIndicators={true}
        infiniteLoop={true}
        autoPlay={true}
      >
        {/* <div>
          <img
            src="https://i.ibb.co/4m9zL2Y/aa8947d0a8f758f2.jpg"
            alt=""
            style={this.img}
            className="d-block w-100"
          />
        </div> */}
        <div>
          <img
            src="https://i.ibb.co/tq9j6V7/4dfdf0c59f26c4a1.jpg"
            alt=""
            style={this.img}
            className="d-block w-100"
          />
        </div>
        <div>
          <img
            src="https://i.ibb.co/vQZhcvT/68af1ae7331acd1c.jpg"
            alt=""
            style={this.img}
            className="d-block w-100"
          />
        </div>
        <div onClick={() => this.props.onCart()}>
          <img
            src="https://i.ibb.co/qxHzVsp/30d7dffe1a1eae09.jpg"
            alt=""
            style={this.img}
            className="d-block w-100"
          />
        </div>
      </Carousel>
    );
  }
}

export default Banner;
