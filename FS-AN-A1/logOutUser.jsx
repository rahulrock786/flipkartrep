import React, { Component } from "react";
import { logoutUser } from "../../services/flipkartService";
class LogOutUser extends Component {
  async componentDidMount() {
    try {
      sessionStorage.removeItem("token");
      //   await logoutUser();
      window.location = "/home";
    } catch (error) {
      console.log(error);
    }
  }
  render() {
    return null;
  }
}

export default LogOutUser;
