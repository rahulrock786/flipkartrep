import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import ProductDisplay from "./productDisplay";
import Home from "./home";
import CheckOut from "./checkout";
import SelectedProduct from "./selectedProduct";

import EditProduct from "./admin/editProduct";
import Admindashboard from "./admin/dashboard";
import LoginUser from "./loginUser";
import LogOutUser from "./logOutUser";

class FlipkartRouter extends Component {
  state = { search: "" };
  handleSearch = (search) => {
    this.setState({ search });
  };
  handleSubmit = (props) => {
    console.log(props);
  };
  render() {
    return (
      <Switch>
        <Route path="/admin/editProduct" component={EditProduct} />
        <Route path="/admin" component={Admindashboard} />

        <Route path="/login" component={LoginUser} />
        <Route path="/logout" component={LogOutUser} />
        <Route path="/home/checkout" component={CheckOut} />
        <Route
          path="/home/product/:category/:brand/:id"
          component={SelectedProduct}
        />

        <Route
          path="/home/products/:category?/:brand?"
          component={ProductDisplay}
        />

        <Route path="/home" component={Home} exact={true} />
        <Redirect to="/home" />
      </Switch>
    );
  }
}

export default FlipkartRouter;
