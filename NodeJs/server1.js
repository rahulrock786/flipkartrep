const express = require("express");
const app = express();
const fs = require("fs");
const cors = require("cors");
const logout = require("express-passport-logout");
const bodyParser = require("body-parser");
const Json2csvParser = require("json2csv").Parser;
const csv = require("csvtojson");

const jwt = require("jsonwebtoken");
const auth = require("./auth.js")();
const multer = require("multer");
const users = require("./dataFiles/users").users;
const cfg = require("./config.js");
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ credentials: true }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Expose-Headers", "X-Auth-Token");
  next();
});

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // Uploads is the Upload_folder_name
    cb(null, "uploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + ".csv");
  },
});
const upload = multer({ storage: storage }).single("myFile");

const jwtExpirySeconds = 30000;
const port = 2410;

let mobiles = require("./dataFiles/mobiles.json");
let laptops = require("./dataFiles/laptops.json");
let cameras = require("./dataFiles/cameras.json");

app.listen(port, () => console.log("Listening on Ports :", port));

//Passport Login Api
app.post("/login", function (req, res) {
  if (req.body.email) {
    let email = req.body.email;
    console.log(users);
    let user = users.find(function (u) {
      return u.email === email;
    });
    if (user) {
      let payload = {
        id: user.id,
        role: user.role,
      };
      let token = jwt.sign(payload, cfg.jwtSecret, {
        algorithm: "HS256",
        expiresIn: jwtExpirySeconds,
      });
      res.setHeader("X-Auth-Token", token);
      res.json({ success: true, token: "bearer " + token });
    } else {
      res.sendStatus(401);
    }
  } else {
    res.sendStatus(401);
  }
});

app.get("/user", auth.authenticate(), function (req, res) {
  let user = users.find((obj) => obj.id === req.user.id);
  if (user) {
    res.send(user);
  } else {
    res.sendStatus(401);
  }
});

//Product Fetch
app.get("/products/:category?/:brand?", function (req, res) {
  let {
    ram: ramSel,
    rating: ratingSel,
    sort: sortSel,
    price: priceSel,
    assured: assuredSel,
    q: search,
    page,
  } = req.query;
  let { category, brand } = req.params;
  page = isNaN(page) ? 1 : page;
  let ram = ramSel ? ramSel.split(",") : [];
  let rating = ratingSel ? ratingSel.split(",") : [];
  let price = priceSel ? priceSel.split(",") : [];

  let mainList = [];
  if (category === "Laptops") {
    let data = laptops.map((obj) => {
      return obj.prod;
    });
    mainList = brand
      ? data.filter((product) => product.brand === brand)
      : [...data];
  }
  if (category === "Cameras") {
    console.log("cameras");
    let data = cameras.map((obj) => {
      return obj.prod;
    });
    mainList = brand
      ? data.filter((product) => product.brand === brand)
      : [...data];
  }
  if (category === "Mobiles") {
    let data = mobiles.map((obj) => {
      return obj.prod;
    });
    mainList = brand
      ? data.filter((product) => product.brand === brand)
      : [...data];
  }
  if (search) {
    let data = [...laptops, ...cameras, ...mobiles];
    mainList = data.map((obj) => {
      return obj.prod;
    });
  }

  //Search Filter
  let dataProcess1 = [];
  if (search !== undefined) {
    mainList.filter(function (obj) {
      let nameArray = obj.name.split(" ");
      for (let i = 0; i < nameArray.length; i++) {
        let serLen = search.length;
        let nameLen = nameArray[i].length;
        if (nameLen >= serLen) {
          let nameString = nameArray[i].substring(0, serLen);
          if (search.toLowerCase() === nameString.toLowerCase()) {
            dataProcess1.push(obj);
            break;
          }
        }
      }
    });
  } else {
    dataProcess1 = [...mainList];
  }

  //Assured Filter
  let dataProcess2 = [];
  if (assuredSel) {
    console.log("assures1 ");
    dataProcess2 = dataProcess1.filter((obj) => {
      if (obj.assured) {
        return obj;
      }
    });
  } else {
    dataProcess2 = [...dataProcess1];
  }
  //Ram && Rating && Price Filter
  let dataProcess3 = [];
  if (ram.length > 0 || price.length > 0 || rating.length > 0) {
    //Ram Filter

    if (ram.length > 0) {
      for (let i = 0; i < ram.length; i++) {
        console.log("Ram ");

        if (ram[i] === ">=6") {
          dataProcess2.map((obj) => {
            if (obj.ram >= 6) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }

        if (ram[i] === "<=4") {
          dataProcess2.map((obj) => {
            if (assuredSel) {
              if (obj.ram <= 4 && obj.assured === assuredSel) {
                let index = dataProcess3.findIndex((x) => x.id === obj.id);
                if (index < 0) dataProcess3.push(obj);
              }
            } else {
              if (obj.ram <= 4) {
                let index = dataProcess3.findIndex((x) => x.id === obj.id);
                if (index < 0) dataProcess3.push(obj);
              }
            }
          });
        }
        if (ram[i] === "<=3") {
          dataProcess2.map((obj) => {
            if (assuredSel) {
              if (obj.ram <= 3 && obj.assured === assuredSel) {
                let index = dataProcess3.findIndex((x) => x.id === obj.id);
                if (index < 0) dataProcess3.push(obj);
              }
            } else {
              if (obj.ram <= 3) {
                let index = dataProcess3.findIndex((x) => x.id === obj.id);
                if (index < 0) dataProcess3.push(obj);
              }
            }
          });
        }
        if (ram[i] === "<=2") {
          dataProcess2.map((obj) => {
            if (assuredSel) {
              if (obj.ram <= 2 && obj.assured === assuredSel) {
                let index = dataProcess3.findIndex((x) => x.id === obj.id);
                if (index < 0) dataProcess3.push(obj);
              }
            } else {
              if (obj.ram <= 2) {
                let index = dataProcess3.findIndex((x) => x.id === obj.id);
                if (index < 0) dataProcess3.push(obj);
              }
            }
          });
        }
      }
    }

    //Rating Filter
    if (rating.length > 0) {
      for (let i = 0; i < rating.length; i++) {
        console.log("Rat ");
        if (rating[i] === ">4") {
          dataProcess2.map((obj) => {
            if (obj.rating > 4) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
        if (rating[i] === ">3") {
          dataProcess2.map((obj) => {
            if (obj.rating > 3) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
        if (rating[i] === ">2") {
          dataProcess2.map((obj) => {
            if (obj.rating > 2) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
        if (rating[i] === ">1") {
          dataProcess2.map((obj) => {
            if (obj.rating > 1) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
      }
    }

    //Price Filter
    if (price.length > 0) {
      for (let i = 0; i < price.length; i++) {
        console.log("price");
        if (price[i] === "0-5000") {
          dataProcess2.map((obj) => {
            if (obj.price <= 5000) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
        if (price[i] === "5000-10000") {
          dataProcess2.map((obj) => {
            if (obj.price > 5000 && obj.price <= 10000) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
        if (price[i] === "10000-20000") {
          dataProcess2.map((obj) => {
            if (obj.price > 10000 && obj.price <= 20000) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
        if (price[i] === ">20000") {
          dataProcess2.map((obj) => {
            if (obj.price > 20000) {
              let index = dataProcess3.findIndex((x) => x.id === obj.id);
              if (index < 0) dataProcess3.push(obj);
            }
          });
        }
      }
    }
  } else {
    dataProcess3 = [...dataProcess2];
  }

  //Sorting Sel
  let dataProcess4 = [];
  if (sortSel) {
    console.log("sortSel1");
    switch (sortSel) {
      case "asc":
        dataProcess4 = dataProcess3.sort(sortByLowToHighPrice);
        break;
      case "desc":
        dataProcess4 = dataProcess3.sort(sortByHighToLowPrice);
        break;
      case "popularity":
        dataProcess4 = dataProcess3.sort(sortByPopularity);
        break;
      default:
        dataProcess3;
    }
  } else {
    dataProcess4 = [...dataProcess3];
  }

  let respArr = pagination(dataProcess4, page);
  let len = dataProcess4.length;
  let quo = Math.floor(len / 4);
  let rem = len % 4;
  //console.log(quo,rem);
  let extra = rem === 0 ? 0 : 1;
  let numofpages = quo + extra;
  let pageInfo = {
    pageNumber: +page,
    numberOfPages: +numofpages,
    numOfItems: 4,
    totalItemCount: dataProcess4.length,
  };

  res.status(200).send({ data: respArr, pageInfo: pageInfo });
});

app.get("/product/:id", function (req, res) {
  let id = req.params.id;
  let list = [...cameras, ...laptops, ...mobiles];
  let index = list.findIndex((obj) => obj.prod.id === id);
  if (index >= 0) {
    let product = list[index];
    console.log(product);
    res.status(200).send(product);
  }
});

//Product Upload
app.post("/addSingleProduct", auth.authenticate(), function (req, res) {
  let user = req.user;
  if (user && user.role === "admin") {
    console.log("admin");
    let newProduct = req.body;
    console.log(newProduct);
    if (newProduct.prod.category === "Mobiles") {
      if (newProduct.prod.category === "Mobiles") {
        console.log("mobiles");
        let index = mobiles.findIndex(
          (mobile) => mobile.prod.id === newProduct.prod.id
        );
        if (index < 0) {
          mobiles.push(newProduct);
          fs.writeFile(
            "./dataFiles/mobiles.json",
            JSON.stringify(mobiles, null, 4),
            (err) => {
              if (err) {
                console.log(`Error writing file: ${err}`);
              }
              res.status(200).send("New Product Added");
            }
          );
        } else {
          res.status(200).send("Duplicat Id Please Give Alternate Id");
        }
      }
    }
    if (newProduct.prod.category === "Laptops") {
      let index = laptops.findIndex(
        (laptop) => laptop.prod.id === newProduct.prod.id
      );
      if (index < 0) {
        laptops.push(newProduct);
        fs.writeFile(
          "./dataFiles/laptops.json",
          JSON.stringify(laptops, null, 4),
          (err) => {
            if (err) {
              console.log(`Error writing file: ${err}`);
            }
            res.status(200).send("Product Added");
          }
        );
      }
    }
    if (newProduct.prod.category === "Cameras") {
      console.log("cameras");

      let index = cameras.findIndex(
        (camera) => camera.prod.id === newProduct.prod.id
      );
      if (index < 0) {
        cameras.push(newProduct);
        fs.writeFile(
          "./dataFiles/cameras.json",
          JSON.stringify(cameras, null, 4),
          (err) => {
            if (err) {
              console.log(`Error writing file: ${err}`);
            }
            res.status(200).send("Product Added");
          }
        );
      } else {
        res.send("Duplicate Product Id");
      }
    }
  }
});

app.post("/addMultipleProduct", auth.authenticate(), async function (req, res) {
  let user = req.user;
  if (user && user.role === "admin") {
    upload(req, res, async function (err) {
      if (err instanceof multer.MulterError) {
        res.status(500).json(err);
      } else if (err) {
        res.status(500).json(err);
      }
      try {
        // let jsonData = csvToJson
        //   .fieldDelimiter(",")
        //   .getJsonFromCsv("./uploads/myFile.csv");
        let jsonData = await csv().fromFile("./uploads/myFile.csv");

        //check keys

        console.log(jsonData);

        let obj2 = { ...jsonData[0] };
        var obj1Keys = [
          "EMI",
          "assured",
          "brand",
          "brandImg",
          "category",
          "details",
          "discount",
          "exchange",
          "id",
          "img",
          "imgList",
          "name",
          "popularity",
          "prevPrice",
          "price",
          "ram",
          "rating",
          "ratingDesc",
        ].sort();
        var obj2Keys = Object.keys(obj2).sort();
        let count = 0;
        obj2Keys.map(function (value) {
          let index = obj1Keys.findIndex((obj) => obj === value);
          if (index >= 0) {
            count++;
          }
        });
        let dataStatus = [];
        console.log(obj1Keys);
        console.log(obj2Keys);

        if (count === 18) {
          if (jsonData && jsonData.length >= 0) {
            jsonData.map(function (value) {
              if (value.category === "Mobiles") {
                let index = mobiles.findIndex(
                  (obj) => obj.prod.id === value.id
                );
                if (index < 0) {
                  mobiles.push({
                    prod: {
                      id: value.id,
                      category: value.category,
                      brand: value.brand,
                      name: value.name,
                      img: value.img,
                      rating: value.img,
                      ratingDesc: value.ratingDesc,
                      details: JSON.parse(value.details),
                      price: value.price,
                      assured: value.assured,
                      prevPrice: value.prevPrice,
                      discount: value.discount,
                      EMI: value.EMI,
                      exchange: value.exchange,
                      ram: value.ram,
                      popularity: value.popularity,
                    },
                    pics: {
                      imgList: JSON.parse(value.imgList),
                      brandImg: value.brandImg,
                    },
                  });
                  dataStatus.push({
                    productId: value.id,
                    category: value.category,
                    status: "Product is Added",
                    reason: "New Data",
                    statusCode: 200,
                  });
                } else {
                  dataStatus.push({
                    productId: value.id,
                    category: value.category,
                    status: "Not added",
                    reason: "Duplicate Id",
                    statusCode: 400,
                  });
                }
              }
              if (value.category === "Laptops") {
                let index = laptops.findIndex(
                  (obj) => obj.prod.id === value.id
                );
                if (index < 0) {
                  laptops.push({
                    prod: {
                      id: value.id,
                      category: value.category,
                      brand: value.brand,
                      name: value.name,
                      img: value.img,
                      rating: value.img,
                      ratingDesc: value.ratingDesc,
                      details: JSON.parse(value.details),
                      price: value.price,
                      assured: value.assured,
                      prevPrice: value.prevPrice,
                      discount: value.discount,
                      EMI: value.EMI,
                      exchange: value.exchange,
                      ram: value.ram,
                      popularity: value.popularity,
                    },
                    pics: {
                      imgList: JSON.parse(value.imgList),
                      brandImg: value.brandImg,
                    },
                  });
                  dataStatus.push({
                    productId: value.id,
                    category: value.category,
                    status: "Product is Added",
                    reason: "New Data",
                    statusCode: 200,
                  });
                } else {
                  dataStatus.push({
                    productId: value.id,
                    category: value.category,
                    status: "Not added",
                    reason: "Duplicate Id",
                    statusCode: 400,
                  });
                }
              }
              if (value.category === "Cameras") {
                let index = cameras.findIndex(
                  (obj) => obj.prod.id === value.id
                );
                if (index < 0) {
                  cameras.push({
                    prod: {
                      id: value.id,
                      category: value.category,
                      brand: value.brand,
                      name: value.name,
                      img: value.img,
                      rating: value.img,
                      ratingDesc: value.ratingDesc,
                      details: JSON.parse(value.details),
                      price: value.price,
                      assured: value.assured,
                      prevPrice: value.prevPrice,
                      discount: value.discount,
                      EMI: value.EMI,
                      exchange: value.exchange,
                      ram: value.ram,
                      popularity: value.popularity,
                    },
                    pics: {
                      imgList: JSON.parse(value.imgList),
                      brandImg: value.brandImg,
                    },
                  });
                  dataStatus.push({
                    productId: value.id,
                    category: value.category,
                    status: "Product is Added",
                    reason: "New Data",
                    statusCode: 200,
                  });
                } else {
                  dataStatus.push({
                    productId: value.id,
                    category: value.category,
                    status: "Not added",
                    reason: "Duplicate Id",
                    statusCode: 400,
                  });
                }
              }
            });
            fs.writeFile(
              "./dataFiles/mobiles.json",
              JSON.stringify(mobiles, null, 4),
              (err) => {
                if (err) {
                  console.log(`Error writing file: ${err}`);
                }
              }
            );
            fs.writeFile(
              "./dataFiles/laptops.json",
              JSON.stringify(laptops, null, 4),
              (err) => {
                if (err) {
                  console.log(`Error writing file: ${err}`);
                }
              }
            );
            fs.writeFile(
              "./dataFiles/cameras.json",
              JSON.stringify(cameras, null, 4),
              (err) => {
                if (err) {
                  console.log(`Error writing file: ${err}`);
                }
              }
            );
            console.log("file Written");
            console.log(dataStatus);
            res.status(200).send(dataStatus);
          } else {
            res.status(404).send("No Data Available");
          }
        } else {
          console.log("keySNotMatching");
          res
            .status(404)
            .send(
              "Csv File Contains Invalid Property Please Upload Valid Csv Files"
            );
        }
      } catch (error) {
        console.log(error);
      }
    });
  }
});

app.put("/editProduct/:id", auth.authenticate(), function (req, res) {
  if (req.user && req.user.role === "admin") {
    console.log("edit");
    let id = req.params.id;
    let editedProduct = req.body;

    if (editedProduct.prod.category === "Mobiles") {
      let index = mobiles.findIndex((mobile) => mobile.prod.id === id);
      mobiles[index] = editedProduct;
      fs.writeFile(
        "./dataFiles/mobiles.json",
        JSON.stringify(mobiles, null, 4),
        (err) => {
          if (err) {
            console.log(`Error writing file: ${err}`);
          }
          res.send("Product Edited Succesfully");
        }
      );
    }
    if (editedProduct.prod.category === "Laptops") {
      let index = laptops.findIndex((laptop) => laptop.prod.id === id);
      laptops[index] = editedProduct;
      fs.writeFile(
        "./dataFiles/laptops.json",
        JSON.stringify(laptops, null, 4),
        (err) => {
          if (err) {
            console.log(`Error writing file: ${err}`);
          }
        }
      );
      res.send("Product Edited Succesfully");
    }
    if (editedProduct.prod.category === "Cameras") {
      let index = cameras.findIndex((camera) => camera.prod.id === id);
      cameras[index] = editedProduct;
      fs.writeFile(
        "./dataFiles/cameras.json",
        JSON.stringify(cameras, null, 4),
        (err) => {
          if (err) {
            console.log(`Error writing file: ${err}`);
          }
        }
      );
      res.send("Product Edited Succesfully");
    }
  }
});

//Various Reports Api

app.get("/download/csv/:reportType", function (req, res) {
  if ("admin" === "admin") {
    let { reportType } = req.params;
    let jsonData = [];

    if (reportType) {
      if (reportType === "allProductReport") {
        let allProducts = [...mobiles, ...laptops, ...cameras];
        allProducts.map(function (value) {
          jsonData.push({ ...value.prod, ...value.pics });
        });
      }
      if (reportType === "favReport") {
        let favReport = require("./dataFiles/favReport.json");
        if (favReport) {
          favReport.map(function (value) {
            jsonData.push(value);
          });
        }
      }
      if (reportType === "searchReport") {
        let searchReport = require("./dataFiles/searchReport.json");
        if (searchReport) {
          searchReport.map(function (value) {
            jsonData.push(value);
          });
        }
      }
      if (reportType === "cartReport") {
        let cartReport = require("./dataFiles/cartReport.json");
        if (cartReport) {
          cartReport.map(function (value) {
            jsonData.push(value);
          });
        }
      }
      if (reportType === "userActivityReport") {
        let userActivityReport = require("./dataFiles/userActivityReport.json");
        if (userActivityReport) {
          userActivityReport.map(function (value) {
            jsonData.push(value);
          });
        }
      }
    }
    res.status(200).send(jsonData);
  }
});

app.post("/favReport", auth.authenticate(), function (req, res) {
  console.log(req.body);
  let data = req.body;
  if (req.user) {
    let favReport = require("./dataFiles/favReport.json");
    let index = favReport.findIndex((obj) => obj.id === data.id);
    if (index < 0) {
      favReport.push({ ...data, volume: 1 });
    } else {
      let report = favReport[index];
      report.volume++;
      favReport[index] = report;
    }
    fs.writeFile(
      "./dataFiles/favReport.json",
      JSON.stringify(favReport, null, 4),
      (err) => {
        if (err) {
          console.log(`Error writing file: ${err}`);
          res.status(400).send("Error");
        }
        res.status(200).send("Success");
      }
    );
  }
});

app.post("/searchReport", function (req, res) {
  console.log(req.body);
  let data = req.body;

  let searchReport = require("./dataFiles/searchReport.json");
  let index = searchReport.findIndex((obj) => obj.keyword === data.keyword);
  if (index < 0) {
    searchReport.push({ ...data, volume: 1 });
  } else {
    let report = searchReport[index];
    report.volume++;
    searchReport[index] = report;
  }
  fs.writeFile(
    "./dataFiles/searchReport.json",
    JSON.stringify(searchReport, null, 4),
    (err) => {
      if (err) {
        console.log(`Error writing file: ${err}`);
        res.status(400).send("Success");
      }
      res.status(200).send("Success");
    }
  );
});

app.post("/cartReport", auth.authenticate(), function (req, res) {
  console.log(req.body);
  let data = req.body;
  if (req.user) {
    let cartReport = require("./dataFiles/cartReport.json");
    let index = cartReport.findIndex((obj) => obj.id === data.id);
    if (index < 0) {
      cartReport.push({ ...data, volume: 1 });
    } else {
      let report = cartReport[index];
      report.volume++;
      cartReport[index] = report;
    }
    fs.writeFile(
      "./dataFiles/cartReport.json",
      JSON.stringify(cartReport, null, 4),
      (err) => {
        if (err) {
          console.log(`Error writing file: ${err}`);
          res.status(400).send("Success");
        }
        res.status(200).send("Success");
      }
    );
  }
});

app.post("/userActivityReport", auth.authenticate(), function (req, res) {
  console.log(req.body);
  let data = req.body;
  if (req.user) {
    let userActivityReport = require("./dataFiles/userActivityReport.json");
    let index = userActivityReport.findIndex((obj) => obj.id === data.id);
    if (index < 0) {
      userActivityReport.push(data);
    }
    fs.writeFile(
      "./dataFiles/userActivityReport.json",
      JSON.stringify(userActivityReport, null, 4),
      (err) => {
        if (err) {
          console.log(`Error writing file: ${err}`);
        }
        res.status(200).send("Ok");
      }
    );
  }
});

app.delete("/user", auth.authenticate(), logout());

function sortByPopularity(prod1, prod2) {
  if (prod1.popularity > prod2.popularity) return 1;
  if (prod1.popularity == prod2.popularity) return 0;
  if (prod1.popularity < prod2.popularity) return -1;
}
function sortByHighToLowPrice(prod1, prod2) {
  if (prod1.price > prod2.price) return -1;
  if (prod1.price === prod2.price) return 0;
  if (prod1.price < prod2.price) return 1;
}
function sortByLowToHighPrice(prod1, prod2) {
  if (prod1.price > prod2.price) return 1;
  if (prod1.price === prod2.price) return 0;
  if (prod1.price < prod2.price) return -1;
}

function pagination(obj, page) {
  const postCount = obj.length;
  const perPage = 4;
  //const pageCount = Math.ceil(postCount / perPage);
  var resArr = obj;
  resArr = resArr.slice(page * 4 - 4, page * 4);
  return resArr;
}
