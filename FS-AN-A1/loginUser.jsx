import React, { Component } from "react";
import { Formik, Form, Field, isEmptyArray } from "formik";
import * as Yup from "yup";
import FlipkartNavbar from "./header";
import { getCurrentUser, loginUser } from "../../services/flipkartService";
class LoginUser extends Component {
  state = {
    token: "",
  };

  handleSubmit = async (values, { ...rest }) => {
    let loginData = {
      email: values.email,
      password: values.password,
    };
    let tokenGranted = false;

    try {
      let response = await loginUser(loginData);
      const { data } = response;
      var token = data.token;
      sessionStorage.setItem("token", JSON.stringify(token));
      tokenGranted = true;
    } catch (error) {
      if (error) throw error;
    }
    if (tokenGranted) {
      try {
        let response = await getCurrentUser(token);
        alert(`Your SuccesFully Logged as ${response.data.role}`);
        if (response.data.role === "admin") window.location = "/admin";
        if (response.data.role === "user") window.location = "/user";
      } catch (error) {}
    }
  };

  render() {
    return (
      <>
        <div className="row m-auto">
          <div className="col-12">
            <FlipkartNavbar />
          </div>
        </div>

        <Formik
          initialValues={{ email: "", password: "" }}
          validationSchema={Yup.object({
            email: Yup.string()
              .required("Email is required")
              .email("Invalid Email"),
            password: Yup.string().required("Required"),
          })}
          onSubmit={this.handleSubmit}
        >
          {({ values, handleChange, errors }) => {
            return (
              <Form>
                <div
                  className="row m-auto border py-5 bg-warning"
                  style={{ maxWidth: 450 }}
                >
                  <div className="col-12 ">
                    <div className="h4 text-center">Login</div>
                    <div className="form-group ">
                      <label className="form-check-label" htmlFor="Email">
                        Email
                      </label>
                      <Field
                        type="text"
                        name="email"
                        value={values.email}
                        onChange={handleChange}
                        className="form-control"
                      />
                    </div>
                    {errors.email ? (
                      <div className="text-danger">{errors.email}</div>
                    ) : null}
                    <div className="form-group">
                      <label className="form-check-label" htmlFor="Password">
                        Password
                      </label>
                      <Field
                        type="password"
                        name="password"
                        value={values.password}
                        onChange={handleChange}
                        className="form-control"
                      />
                    </div>
                    {errors.password ? (
                      <div className="text-danger">{errors.password}</div>
                    ) : null}
                  </div>
                  <div className="col-12 text-center">
                    <button type="submit" className="btn btn-primary">
                      Sign In
                    </button>
                  </div>
                </div>
              </Form>
            );
          }}
        </Formik>
      </>
    );
  }
}

export default LoginUser;
