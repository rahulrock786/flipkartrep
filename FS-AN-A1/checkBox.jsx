import React, { Component } from "react";

class CheckBox extends Component {
  state = { show: true };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { data, onCheckBoxEvent, type } = this.props;
    let index = data.findIndex((n1) => n1.name === input.name);
    data[index].selected = input.checked;
    onCheckBoxEvent(data, type);
  };
  handleSlideShow = () => {
    this.setState({ show: !this.state.show });
  };

  render() {
    const { type, data } = this.props;
    return (
      <>
        <div className="col-12">
          <div
            className="row ml-1 pb-2"
            style={{
              fontSize: 13,
              fontWeight: 500,
              textTransform: "uppercase",
              letterSpacing: 0.3,
              display: "inlineBlock",
            }}
            onClick={() => this.handleSlideShow()}
          >
            <div className="col-8">{this.props.type}</div>
            {this.state.show ? (
              <div className="col-3 text-right">
                <span>
                  <i
                    style={{
                      fontSize: 10,
                      color: "lightgrey",
                      cursor: "pointer",
                    }}
                    className="fa fa-chevron-down"
                  ></i>
                </span>
              </div>
            ) : (
              <div className="col-3 text-right">
                <span>
                  <i
                    style={{
                      fontSize: 10,
                      color: "lightgrey",
                      cursor: "pointer",
                    }}
                    className="fa fa-chevron-up"
                  ></i>
                </span>
              </div>
            )}
          </div>

          {this.state.show
            ? data && data.length >= 0
              ? data.map((item, index) => (
                  <div className="row form-check ml-1 pb-1" key={index}>
                    <div
                      className="checkbox"
                      style={{
                        verticalAlign: "middle",
                        fontSize: 15,
                        paddingLeft: 11,
                        color: "#212121",
                        display: "inline-block",
                        width: "calc(100% - 25px)",
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        lineHeight: 1,
                        cursor: "pointer",
                      }}
                    >
                      <label htmlFor={item.name}>
                        <input
                          name={item.name}
                          value={item.value}
                          type="checkbox"
                          id={item}
                          onChange={this.handleChange}
                          checked={item.selected}
                        />
                        &nbsp;{item.name}
                        {type === "Customer Rating" ? (
                          <>
                            <i className="fa fa-star"></i> & above
                          </>
                        ) : (
                          ""
                        )}
                      </label>
                    </div>
                  </div>
                ))
              : ""
            : ""}
        </div>
      </>
    );
  }
}

export default CheckBox;
