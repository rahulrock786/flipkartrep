import React, { Component } from "react";
import "./header.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import queryString from "query-string";
import { getCurrentUser } from "../../services/flipkartService";

class FlipkartNavbar extends Component {
  state = { search: "", user: null };

  async componentDidMount() {
    try {
      let response = await getCurrentUser();
      console.log(response);
      let user = response.data;
      this.setState({ user });
    } catch (error) {
      if (error) throw error;
      console.log(error);
    }
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ search: input.value });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamName) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let search = this.state.search;
    this.props.onSubmit(search);
  };
  render() {
    return (
      <>
        <div
          className="row"
          style={{ backgroundColor: "#2874f0", paddingBottom: 10 }}
        >
          <div className="col-lg-2 col-4 mt-1 p-0 text-right">
            <div className="row ml-1 ">
              <Link to="/home">
                <img
                  className="img-fluid"
                  src="https://i.ibb.co/qs8BK6Y/flipkart-plus-4ee2f9.png"
                  style={{ width: 100, cursor: "pointer" }}
                />
              </Link>
            </div>
            <div className="row text-primary ml-1">
              <Link
                to="/home"
                className="text-white"
                href="#"
                style={{ fontSize: 10 }}
              >
                <i>
                  Explore
                  <span style={{ color: "yellow" }}>
                    Plus
                    <img
                      src="https://i.ibb.co/t2WXyzj/plus-b13a8b.png"
                      style={{ width: 10 }}
                    />
                  </span>
                </i>
              </Link>
            </div>
          </div>

          <div className="col-lg-4 col-5 mt-2 p-0">
            <form onSubmit={this.handleSubmit}>
              <input
                className="form-control"
                type="text"
                value={this.props.search}
                onChange={this.handleChange}
                placeholder="Search for products, brands and more"
              />
            </form>
          </div>

          {this.state.user ? (
            <div
              className="col-2 col-lg-1 text-white mt-1 text-center p-0 d-none d-lg-block "
              style={{ minHeight: 20 }}
            >
              <div className="dropdown">
                <div className="dropbtn1" style={{ fontSize: 15 }}>
                  My Account
                  <span>
                    <i
                      id="onhover"
                      style={{ fontSize: 10, color: "lightgrey" }}
                      className="fa fa-chevron-down pl-1"
                    ></i>
                  </span>
                </div>
                <div className="dropdown-content">
                  <div>
                    <Link to="" href="#">
                      My Profile
                    </Link>
                  </div>
                  <div>
                    <Link to="" href="#">
                      Orders
                    </Link>
                  </div>
                  <div>
                    <Link to="" href="#">
                      WishList
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="col"></div>
          )}

          <div
            className="col-1 text-white mt-1  p-0 d-none d-lg-block  text-center"
            style={{ minHeight: 20 }}
          >
            <div className="dropdown ">
              <div className="dropbtn1" style={{ fontSize: 15 }}>
                More
                <i
                  id="onhover"
                  style={{ fontSize: 10, color: "lightgrey" }}
                  className="fa fa-chevron-down pl-1"
                ></i>
              </div>
              <div className="dropdown-content">
                <div>
                  <Link to="" href="#">
                    Notifications
                  </Link>
                </div>
                <div>
                  <Link to="" href="#">
                    Sell on FlipKart
                  </Link>
                </div>
                <div>
                  <Link to="" href="#">
                    24X7 Customer Care
                  </Link>
                </div>
                <div>
                  <Link to="" href="#">
                    Advertise
                  </Link>
                </div>
              </div>
            </div>
          </div>

          <div
            className="col-lg-1 col-3 text-white mt-3  p-0 "
            style={{ cursor: "pointer", fontWeight: "lighter" }}
          >
            <Link
              to="/home/checkout"
              style={{ fontWeight: "lighter", color: "white", marginLeft: 15 }}
            >
              <i className="fa fa-shopping-cart">
                {this.getCartQuantity() !== 0 ? (
                  <span className="badge badge-danger">
                    {this.getCartQuantity()}
                  </span>
                ) : (
                  ""
                )}
              </i>
            </Link>
            &nbsp;&nbsp;
            <Link to="/home/checkout">
              <span style={{ fontWeight: "lighter", color: "white" }}>
                Cart
              </span>
            </Link>
          </div>
          {this.state.user === null ? (
            <div className="col-lg col-12 mt-3 p-0 text-right">
              <Link
                to="/login"
                style={{
                  color: "white",
                  textDecoration: "none",
                  marginRight: 20,
                  fontWeight: 450,
                }}
              >
                Login
              </Link>
            </div>
          ) : (
            <>
              <div className="col-lg-2 col-6 mt-3 p-0 text-right  d-sm-block d-none">
                <span
                  style={{
                    color: "white",
                    textDecoration: "none",
                    marginRight: 20,
                    fontWeight: 450,
                    fontSize: "clamp(12px,2vw,18px)",
                    marginLeft: 5,
                  }}
                >
                  Welcome User {this.state.user.name}
                </span>
              </div>
              <div className="col-lg-2 col-8 mt-3 p-0 text-left  d-sm-none">
                <span
                  style={{
                    color: "white",
                    textDecoration: "none",
                    marginRight: 20,
                    fontWeight: 450,
                    fontSize: "clamp(12px,2vw,15px)",
                    marginLeft: 5,
                  }}
                >
                  Welcome User {this.state.user.name}
                </span>
              </div>
              <div className="col-lg col mt-3 p-0 text-right">
                <Link
                  to="/logout"
                  style={{
                    color: "white",
                    textDecoration: "none",
                    marginRight: 20,
                    fontWeight: 450,
                  }}
                >
                  Logout
                </Link>
              </div>
            </>
          )}
        </div>

        <div
          className="row bg-white"
          style={{
            color: "#212121",
            minHeight: 25,
            fontFamily: "Roboto,Arial,sans-serif",
          }}
        >
          <div className="col-4 text-center" id="onhover">
            <div className="dropdown">
              <div className="dropbtn" style={{ fontSize: 15 }}>
                Mobiles
                <i
                  className="fa fa-chevron-down pl-1"
                  id="onhover"
                  style={{ fontSize: 10, color: "lightgrey" }}
                ></i>
              </div>
              <div className="dropdown-content">
                <div>
                  <Link to="/home/products/Mobiles/Mi?page=1">Mi</Link>
                </div>
                <div>
                  <Link to="/home/products/Mobiles/RealMe?page=1">RealMe</Link>
                </div>
                <div>
                  <Link to="/home/products/Mobiles/Samsung?page=1">
                    Samsung
                  </Link>
                </div>
                <div>
                  <Link to="/home/products/Mobiles/OPPO?page=1">OPPO</Link>
                </div>
                <div>
                  <Link to="/home/products/Mobiles/Apple?page=1">Apple</Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-4 text-center" id="onhover">
            <div className="dropdown">
              <div className="dropbtn" style={{ fontSize: 15 }}>
                Laptops
                <i
                  id="onhover"
                  style={{ fontSize: 10, color: "lightgrey" }}
                  className="fa fa-chevron-down pl-1"
                ></i>
              </div>
              <div className="dropdown-content">
                <div>
                  <Link to="/home/products/Laptops/Apple?page=1">Apple</Link>
                </div>
                <div>
                  <Link to="/home/products/Laptops/HP?page=1">HP</Link>
                </div>
                <div>
                  <Link to="/home/products/Laptops/Dell?page=1">Dell</Link>
                </div>
                <div>
                  <Link to="/home/products/Laptops/Acer?page=1">Acer</Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-4 text-center" id="onhover">
            <div className="dropdown">
              <div className="dropbtn" style={{ fontSize: 15 }}>
                Cameras
                <i
                  id="onhover"
                  style={{ fontSize: 10, color: "lightgrey" }}
                  className="fa fa-chevron-down pl-1"
                ></i>
              </div>
              <div className="dropdown-content">
                <div>
                  <Link to="/home/products/Cameras/DSLR?page=1">DSLR</Link>
                </div>
                <div>
                  <Link to="/home/products/Cameras/Lens?page=1">Lens</Link>
                </div>
                <div>
                  <Link to="/home/products/Cameras/Tripods?page=1">
                    Tripods
                  </Link>
                </div>
                <div>
                  <Link to="/home/products/Cameras/Compact?page=1">
                    Compact
                  </Link>
                </div>
                <div>
                  <Link to="/home/products/Cameras/Accessories?page=1">
                    Accessories
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
  getCartQuantity() {
    let cart = this.props.cart;
    let totalQty = cart.reduce((a, b) => (a = a + b.quantity), 0);
    return totalQty;
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
});
export default connect(mapStateToProps)(FlipkartNavbar);
