import React, { Component } from "react";
import FlipkartNavbar from "./header";
import { connect } from "react-redux";
import queryString from "query-string";

class CheckOut extends Component {
  state = {};
  updateCart = (item, x) => {
    this.props.dispatch({ type: "ADD_CART", item: item, update: x });
  };
  handleSearch = (search) => {
    let { ram, rating, price, sort, assured } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    params = this.addToParams(params, "page", 1);
    params = this.addToParams(params, "q", search);

    this.props.history.push(`/home/products/${params}`);
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamName) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  render() {
    const { cart } = this.props;
    return (
      <div className="container-fluid" style={{ backgroundColor: "#d4d3cf" }}>
        <FlipkartNavbar onSubmit={this.handleSearch} />
        <div className="row mt-2">
          <div className="col-lg-8 col-12 bg-white ml-1  mr-1">
            <div className="row border-bottom">
              <div
                className="col-6 py-3"
                style={{
                  fontSize: 18,
                  fontWeight: 500,
                }}
              >
                My Cart({this.getCartQuantity()})
              </div>
            </div>
            <div>
              {cart && cart.length >= 0
                ? cart.map((item) => (
                    <>
                      <div className="row mt-1 bg-white">
                        <div className="col-lg-2 col-4">
                          <div className="row">
                            <div className="col text-center">
                              <img
                                style={{ width: 50, height: 92 }}
                                src={item.img}
                              />
                            </div>
                          </div>
                          <div className="row mt-1">
                            <div className="col-12 text-center">
                              <button
                                className="btn btn-sm"
                                style={{
                                  backgroundColor: "white",
                                  borderColor: "#e0e0e0",
                                  cursor: "auto",
                                  borderRadius: "50%",
                                  cursor: "pointer",
                                  fontSize: 12,
                                }}
                                disabled={item.quantity <= 1}
                                onClick={() => this.updateCart(item, -1)}
                              >
                                -
                              </button>
                              <span id="rect">{item.quantity}</span>
                              <button
                                className="btn btn-sm"
                                style={{
                                  backgroundColor: "white",
                                  borderColor: "#e0e0e0",
                                  cursor: "auto",
                                  borderRadius: "50%",
                                  fontSize: 12,
                                }}
                                onClick={() => this.updateCart(item, 1)}
                              >
                                +
                              </button>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-5 col-4">
                          <div className="row">
                            <div
                              className="col"
                              style={{ fontSize: 16, color: "#212121" }}
                            >
                              {item.name}
                            </div>
                          </div>
                          <div className="row">
                            <div
                              className="col"
                              style={{ color: "#878787", fontSize: 14 }}
                            >
                              {item.brand} &nbsp;&nbsp;
                              <span>
                                <img
                                  className="img-fluid"
                                  src="https://i.ibb.co/XCQBqSr/fa-8b4b59.png"
                                  style={{ width: 70 }}
                                />
                              </span>
                            </div>
                          </div>
                          <div className="row mt-2">
                            <div className="col" style={{ fontWeight: 500 }}>
                              {item.price} &nbsp;
                              <span
                                style={{
                                  textDecoration: "line-through",
                                  fontSize: 16,
                                  color: "#878787",
                                }}
                              >
                                {item.prevPrice}
                              </span>
                              &nbsp;
                              <span
                                style={{
                                  color: "#388e3c",
                                  fontSize: 16,
                                  fontWeight: 500,
                                }}
                              >
                                {item.discount}%
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-3 col-4">
                          <div className="row">
                            <div className="col" style={{ fontSize: 14 }}>
                              {" "}
                              Delivery in 2 days | Free{" "}
                              <span style={{ textDecoration: "line-through" }}>
                                ₹40
                              </span>
                            </div>
                          </div>
                          <div className="row">
                            <div
                              className="col"
                              style={{ fontSize: 12, color: "gray" }}
                            >
                              10 Days Replacement Policy{" "}
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr />
                    </>
                  ))
                : ""}
            </div>
          </div>
          <div className="col-lg-3 col-12 ml-1">
            <div class="row bg-white">
              <div class="col">
                <div
                  class="row border-bottom"
                  style={{
                    fontSize: 14,
                    fontWeight: 500,
                    color: "#878787",
                    minHeight: 47,
                  }}
                >
                  <div class="col pt-2">PRICE DETAILS</div>
                </div>
                <div class="row pt-2 pb-2">
                  <div class="col-6">Price({this.getCartQuantity()} items)</div>
                  <div class="col-6 text-right">₹{this.getTotalPrice()}</div>
                </div>
                <div class="row pt-2 pb-2">
                  <div class="col-6"> Delivery </div>
                  <div class="col-6 text-success text-right"> FREE </div>
                </div>
                <div
                  class="row pt-2 pb-2"
                  style={{
                    fontWeight: 500,
                    borderTop: "1px dashed e0e0e 0",
                    fontSize: 18,
                  }}
                >
                  <div class="col-6"> Total Payable </div>
                  <div class="col-6 text-right">₹{this.getTotalPrice()}</div>
                </div>
              </div>
            </div>
            <div class="row mt-2">
              <div class="col-1">
                <img
                  src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/shield_435391.svg"
                  style={{ width: 25 }}
                />
              </div>
              <div
                class="col-9"
                style={{
                  fontSize: 14,
                  textAlign: "left",
                  fontWeight: 500,
                  display: "inline-block",
                  color: "#878787",
                }}
              >
                {" "}
                Safe and Secure Payments.Easy returns.100% Authentic products.{" "}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  getCartQuantity() {
    let cart = this.props.cart;
    let totalQty = cart.reduce((a, b) => (a = a + b.quantity), 0);
    return totalQty;
  }
  getTotalPrice() {
    let cart = this.props.cart;
    let totalQty = cart.reduce((a, b) => (a = a + b.quantity * b.price), 0);
    return totalQty;
  }
}
const mapStateToProps = (state) => ({
  cart: state.cart,
});
export default connect(mapStateToProps)(CheckOut);
