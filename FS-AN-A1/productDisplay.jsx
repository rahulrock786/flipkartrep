import React, { Component } from "react";
import * as flipkartService from "../../services/flipkartService";
import FlipkartNavbar from "./header";
import CheckBox from "./checkBox";
import { Link } from "react-router-dom";
import Pagination from "../common/pagination";
import { paginate } from "./../utils/paginate";
import queryString from "query-string";
import "./productDisplay.css";

import { postFavReport } from "./../../services/flipkartService";
import { postUrl } from "../common/apiCall";

class ProductDisplay extends Component {
  state = {
    ram: [
      { name: "6GB and More", value: ">=6" },
      { name: "4GB", value: "<=4" },
      { name: "3GB", value: "<=3" },
      { name: "2GB ", value: "<=2" },
    ],
    rating: [
      { name: "4", value: ">4" },
      { name: "3", value: ">3" },
      { name: "2", value: ">2" },
      { name: "1 ", value: ">1" },
    ],
    price: [
      { name: "0-5000", value: "0-5000" },
      { name: "5000-10000", value: "5000-10000" },
      { name: "10000-20000", value: "10000-20000" },
      { name: ">20000", value: ">20000" },
    ],

    assured: false,

    items: [],

    sortType: [
      { name: "Popularity", value: "popularity" },
      { name: "Price High To Low", value: "desc" },
      { name: "Price Low To High", value: "asc" },
    ],
    sortSel: "",
    loading: false,
    pageInfo: {},
  };

  constructor(props) {
    super(props);
    this.state.loading = false;
  }

  async componentDidMount() {
    try {
      let { category, brand } = this.props.match.params;

      let params = this.props.location.search;
      let url = this.props.match.url + params;
      postUrl(url);
      const response = await flipkartService.getAllProducts(
        category,
        brand,
        params
      );
      console.log(response);
      this.setState({
        loading: true,
        items: response.data.data,
        pageInfo: response.data.pageInfo,
      });
    } catch (error) {}
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.match.params !== this.props.match.params) {
      let { category, brand } = this.props.match.params;
      let params = this.props.location.search;
      let url = this.props.match.url + params;
      postUrl(url);
      try {
        const response = await flipkartService.getAllProducts(
          category,
          brand,
          params
        );
        this.setState({
          items: response.data.data,
          pageInfo: response.data.pageInfo,
          loading: true,
        });
      } catch (error) {}
    }
  }
  handleChangePage = (currentPage) => {
    let { category, brand } = this.props.match.params;
    let params = "";
    let { ram, rating, price, page, sort, q, assured } = queryString.parse(
      this.props.location.search
    );
    params = this.addToParams(params, "page", currentPage);
    if (q) params = this.addToParams(params, "q", q);
    if (ram) params = this.addToParams(params, "ram", ram);
    if (rating) params = this.addToParams(params, "rating", rating);
    if (price) params = this.addToParams(params, "price", price);
    if (sort) params = this.addToParams(params, "sort", sort);
    if (assured) params = this.addToParams(params, "assured", assured);
    this.setState({ loading: false });

    if (category === undefined && brand === undefined) {
      this.props.history.push(`/home/products/${params}`);
    }
    if (category !== undefined && brand === undefined) {
      this.props.history.push(`/home/products/${category}/${params}`);
    } else if (category !== undefined && brand !== undefined) {
      this.props.history.push(`/home/products/${category}/${brand}${params}`);
    }
  };
  handleNextPage = (currentPage) => {
    let { category, brand } = this.props.match.params;
    let { ram, rating, price, page, sort, q, assured } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    params = this.addToParams(params, "page", +currentPage + 1);
    if (q) params = this.addToParams(params, "q", q);
    if (ram) params = this.addToParams(params, "ram", ram);
    if (rating) params = this.addToParams(params, "rating", rating);
    if (price) params = this.addToParams(params, "price", price);
    if (sort) params = this.addToParams(params, "sort", sort);
    if (assured) params = this.addToParams(params, "assured", assured);
    this.setState({ loading: false });
    if (category === undefined && brand === undefined) {
      this.props.history.push(`/home/products/${params}`);
    }
    if (category !== undefined && brand === undefined)
      this.props.history.push(`/home/products/${category}/${params}`);
    else if (category !== undefined && brand !== undefined) {
      this.props.history.push(`/home/products/${category}/${brand}${params}`);
    }
  };
  handleSort = (currentsortSel) => {
    let previousSortSel = this.state.sortSel;
    let { category, brand } = this.props.match.params;
    let { ram, rating, price, page, sort, q, assured } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    if (currentsortSel !== previousSortSel) {
      params = this.addToParams(params, "sort", currentsortSel);
      this.setState({ sortSel: currentsortSel });
    } else {
      this.setState({ sortSel: "" });
    }

    if (page) params = this.addToParams(params, "page", page);
    if (q) params = this.addToParams(params, "q", q);
    if (ram) params = this.addToParams(params, "ram", ram);
    if (rating) params = this.addToParams(params, "rating", rating);
    if (price) params = this.addToParams(params, "price", price);
    if (assured) params = this.addToParams(params, "assured", assured);
    this.setState({ loading: false });
    if (category === undefined && brand === undefined) {
      this.props.history.push(`/home/products/${params}`);
    }
    if (category !== undefined && brand === undefined)
      this.props.history.push(`/home/products/${category}/${params}`);
    else if (category !== undefined && brand !== undefined) {
      this.props.history.push(`/home/products/${category}/${brand}${params}`);
    }
  };
  handleCheckBox = (data, type) => {
    console.log(data);
    let dataArray = data.filter(function (value) {
      if (value.selected) return value.name;
    });
    let nameArray = dataArray.map(function (x) {
      return x.value;
    });
    let nameString = nameArray.join(",");

    let { ram, rating, price, page, sort, q, assured } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    params = this.addToParams(params, "page", 1);
    if (type === "Ram") {
      if (q) params = this.addToParams(params, "q", q);

      if (nameString !== "") {
        params = this.addToParams(params, "ram", nameString);
      }
      if (rating) params = this.addToParams(params, "rating", rating);
      if (price) params = this.addToParams(params, "price", rating);
      if (sort) params = this.addToParams(params, "sort", sort);
      if (assured) params = this.addToParams(params, "assured", assured);
    }
    if (type === "Customer Rating") {
      if (q) params = this.addToParams(params, "q", q);

      if (nameString !== "") {
        params = this.addToParams(params, "rating", nameString);
      }
      if (ram) params = this.addToParams(params, "ram", ram);
      if (price) params = this.addToParams(params, "price", price);
      if (sort) params = this.addToParams(params, "sort", sort);
      if (assured) params = this.addToParams(params, "assured", assured);
    }
    if (type === "Price") {
      if (q) params = this.addToParams(params, "q", q);

      if (nameString !== "") {
        params = this.addToParams(params, "price", nameString);
      }
      if (rating) params = this.addToParams(params, "rating", rating);
      if (ram) params = this.addToParams(params, "ram", ram);
      if (sort) params = this.addToParams(params, "sort", sort);
      if (assured) params = this.addToParams(params, "assured", assured);
    }
    let { category, brand } = this.props.match.params;
    this.setState({ loading: false });
    if (category === undefined && brand === undefined) {
      this.props.history.push(`/home/products/${params}`);
    }
    if (category !== undefined && brand === undefined)
      this.props.history.push(`/home/products/${category}/${params}`);
    else if (category !== undefined && brand !== undefined) {
      this.props.history.push(`/home/products/${category}/${brand}${params}`);
    }
  };

  handleLiked = async (product) => {
    try {
      let user = await flipkartService.getCurrentUser();
      if (user !== null) {
        let items = [...this.state.items];
        let index = items.findIndex((item) => item.id === product.id);
        let item = items[index];

        if (item.liked === undefined) {
          let data = {
            id: product.id,
            category: product.category,
            name: product.name,
            img: product.img,
          };
          try {
            let response = await postFavReport(data);
            if (response) {
              item.liked = !item.liked;
              this.setState({ items });
            }
          } catch (error) {}
        } else {
          item.liked = !item.liked;
          this.setState({ items });
        }
      }
    } catch (error) {
      alert("Please Login First");
    }
  };
  handleSearch = async (search) => {
    let { ram, rating, price, sort, assured } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    params = this.addToParams(params, "page", 1);
    params = this.addToParams(params, "q", search);

    if (ram) params = this.addToParams(params, "ram", ram);
    if (rating) params = this.addToParams(params, "rating", rating);
    if (price) params = this.addToParams(params, "price", price);
    if (sort) params = this.addToParams(params, "sort", sort);
    if (assured) params = this.addToParams(params, "assured", assured);
    let data = {
      keyword: search,
    };
    try {
      let response = await flipkartService.postSearchReport(data);
      if (response) {
        this.setState({ loading: false });
        this.props.history.push(`/home/products${params}`);
      }
    } catch (error) {}
  };
  handleSelectedProduct = (item) => {
    let { category, brand } = this.props.match.params;
    console.log(brand);
    this.setState({ loading: false });
    if (brand === undefined) {
      this.props.history.push(
        `/home/product/${item.category}/${item.brand}/${item.id}`
      );
    } else {
      this.props.history.push(
        `/home/product/${item.category}/${item.brand}/${item.id}`
      );
    }
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ assured: input.checked });
    let { ram, rating, price, sort, page, q } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    if (page) params = this.addToParams(params, "page", 1);
    if (q) params = this.addToParams(params, "q", q);
    if (ram) params = this.addToParams(params, "ram", ram);
    if (rating) params = this.addToParams(params, "rating", rating);
    if (price) params = this.addToParams(params, "price", price);
    if (sort) params = this.addToParams(params, "sort", sort);
    let { category, brand } = this.props.match.params;
    this.setState({ loading: false });
    if (input.checked === true) {
      params = this.addToParams(params, "assured", input.checked);
      if (category === undefined && brand === undefined) {
        this.props.history.push(`/home/products/${params}`);
      }
      if (category !== undefined && brand === undefined)
        this.props.history.push(`/home/products/${category}/${params}`);
      else if (category !== undefined && brand !== undefined) {
        this.props.history.push(`/home/products/${category}/${brand}${params}`);
      }
    } else {
      if (category === undefined && brand === undefined) {
        this.props.history.push(`/home/products/${params}`);
      }
      if (category !== undefined && brand === undefined)
        this.props.history.push(`/home/products/${category}/${params}`);
      else if (category !== undefined && brand !== undefined) {
        this.props.history.push(`/home/products/${category}/${brand}${params}`);
      }
    }
  };
  makeCBStrucure(items, selected) {
    let temp = [];
    items.map((item) =>
      temp.push({ value: item.value, name: item.name, selected: false })
    );
    let cnames = selected.split(",");
    for (let i = 0; i < cnames.length; i++) {
      let index = temp.findIndex((item) => item.value === cnames[i]);
      if (index >= 0) temp[index].selected = !temp[index].selected;
    }
    return temp;
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamName) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  render() {
    const {
      ram,
      rating,
      price,
      items,
      pageInfo,
      sortSel,
      sortType,
      loading,
    } = this.state;
    const { numOfItems, pageNumber, totalItemCount, numberOfPages } = pageInfo;
    let {
      ram: ramSel,
      rating: ratingSel,
      price: priceSel,
      assured,
      q: search,
    } = queryString.parse(this.props.location.search);
    ramSel = ramSel ? ramSel : "";
    ratingSel = ratingSel ? ratingSel : "";
    priceSel = priceSel ? priceSel : "";
    let ramCheckBox = this.makeCBStrucure(ram, ramSel);
    let ratingCheckBox = this.makeCBStrucure(rating, ratingSel);
    let priceCheckBox = this.makeCBStrucure(price, priceSel);

    // const items = paginate(products, pageNumber, numOfItems);
    return loading ? (
      <div className="container-fluid" style={{ backgroundColor: "#d4d3cf" }}>
        <FlipkartNavbar onSubmit={this.handleSearch} />
        <div className="row mt-2">
          {/* "Left Panel" */}
          <div className="col-2 ml-2 d-none d-lg-block border">
            <div className="row bg-white border-bottom pt-2 pb-2">
              <div
                className="col"
                style={{
                  fontSize: 18,
                  textTransform: "capitalize",
                  width: "67%",
                  fontWeight: 500,
                }}
              >
                Filters
              </div>
            </div>
            <div className="row bg-white pt-2 pb-2">
              <div className="col-9">
                <div className="checkbox">
                  <label>
                    <input
                      type="checkbox"
                      onChange={this.handleChange}
                      name="assured"
                      checked={assured}
                    />
                    &nbsp;
                    <img
                      src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png"
                      style={{ width: 80, height: 21 }}
                    />
                  </label>
                </div>
              </div>
              <div className="col-3 text-center">
                <span
                  className="border p-1"
                  style={{ borderRadius: "50%", fontSize: 8 }}
                >
                  ?
                </span>
              </div>
            </div>
            {items && items.length > 0 ? (
              items[0].category !== "Cameras" ? (
                <div className="row bg-white border-top">
                  <CheckBox
                    type="Ram"
                    data={ramCheckBox}
                    onCheckBoxEvent={this.handleCheckBox}
                  />
                </div>
              ) : (
                ""
              )
            ) : (
              ""
            )}
            <div className="row bg-white border-top">
              <CheckBox
                type="Customer Rating"
                data={ratingCheckBox}
                onCheckBoxEvent={this.handleCheckBox}
              />
            </div>

            <div className="row bg-white border-top">
              <CheckBox
                type="Price"
                data={priceCheckBox}
                onCheckBoxEvent={this.handleCheckBox}
              />
            </div>
          </div>
          {/* //Main Panel */}
          <div className="col-lg-9 col-12 bg-white ml-1 border">
            <div className="row">
              <div className="col">
                <nav
                  aria-label="breadcrumb"
                  style={{ fontSize: 10, backgroundColor: "white" }}
                >
                  <ol className="breadcrumb bg-white">
                    <li className="breadcrumb-item">
                      <Link to="/home">Home</Link>
                    </li>
                    {items && items.length > 0 ? (
                      <li className="breadcrumb-item">
                        <Link to={"/home/products/" + items[0].category}>
                          {items[0].category}
                        </Link>
                      </li>
                    ) : (
                      ""
                    )}
                    {items && items.length > 0 ? (
                      <li
                        aria-current="page"
                        className="breadcrumb-item active"
                      >
                        {items[0].brand}
                      </li>
                    ) : (
                      ""
                    )}
                  </ol>
                </nav>
              </div>
            </div>
            <div className="row ml-2">
              <div className="col">
                {" "}
                {this.props.match.params.category
                  ? this.props.match.params.brand
                    ? this.props.match.params.brand +
                      " " +
                      this.props.match.params.category
                    : this.props.match.params.category
                  : items && items.length >= 0
                  ? search
                    ? `'${search}'` + " search products"
                    : ""
                  : "No Such Products"}
              </div>
            </div>
            <div className="row pb-1 border-bottom " style={{ fontSize: 14 }}>
              <div className="col-2 d-none d-lg-block ml-2">
                <strong>Sort By</strong>
              </div>
              {sortType.map((x) =>
                x.value === sortSel ? (
                  <div
                    style={{ cursor: "pointer", borderBottom: 5 }}
                    className="col-2  d-none d-lg-block text-primary"
                    onClick={() => this.handleSort(x.value)}
                  >
                    {x.name}
                  </div>
                ) : (
                  <div
                    style={{ cursor: "pointer", borderBottom: 5 }}
                    className="col-2  d-none d-lg-block"
                    onClick={() => this.handleSort(x.value)}
                  >
                    {x.name}
                  </div>
                )
              )}
            </div>
            {items && items.length >= 0
              ? items.map((item, index) => (
                  <div key={index}>
                    <div className="row">
                      <div className="col-lg-2 col-9 text-center my-auto">
                        <img
                          className="img-thumbnail"
                          style={{
                            cursor: "pointer",
                            height: 200,
                            border: "none",
                          }}
                          src={item.img}
                          onClick={() => this.handleSelectedProduct(item)}
                        />
                      </div>

                      <div className="col-lg-1 col-2 text-secondary">
                        {item.liked ? (
                          <i
                            className="fa fa-heart"
                            style={{ color: "red" }}
                            onClick={() => this.handleLiked(item)}
                          ></i>
                        ) : (
                          <i
                            className="fa fa-heart"
                            onClick={() => this.handleLiked(item)}
                          ></i>
                        )}
                      </div>
                      <div className="col-lg-5 col-12 text-left">
                        <div className="row">
                          <div
                            className="col"
                            style={{ fontSize: 16, cursor: "pointer" }}
                          >
                            <Link
                              to={`/home/product/${item.category}/${item.brand}/${item.id}`}
                            >
                              <span id="namecss">{item.name}</span>
                            </Link>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col">
                            <span
                              style={{
                                lineHeight: "normal",
                                display: "inline-block",
                                color: "#fff",
                                padding: "2px 4px 2px 6px",
                                borderRadius: "3px",
                                fontWeight: 500,
                                fontSize: 12,
                                verticalAlign: "middle",
                                backgroundColor: "#388e3c",
                              }}
                            >
                              <strong>
                                {item.rating}&nbsp;
                                {/* <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" /> */}
                              </strong>
                            </span>
                            &nbsp;
                            <span className="text-muted">
                              {item.ratingDesc}
                            </span>
                          </div>
                        </div>
                        {item.details && item.details.length >= 0
                          ? item.details.map((detail, index) => (
                              <ul
                                key={index}
                                style={{
                                  color: "#c2c2c2",
                                  fontSize: 12,
                                  display: "inline",
                                  padding: "0%",
                                }}
                              >
                                <li>{detail}</li>
                              </ul>
                            ))
                          : ""}
                      </div>
                      <div className="col-lg-3 col-12">
                        <div className="row">
                          <div className="col" style={{ fontSize: 25 }}>
                            <strong>₹{item.price}</strong>&nbsp;
                            {item.assured ? (
                              <span>
                                <img
                                  className="img-fluid"
                                  src="https://i.ibb.co/XCQBqSr/fa-8b4b59.png"
                                  style={{ width: 70 }}
                                />
                              </span>
                            ) : (
                              ""
                            )}
                          </div>
                        </div>
                        <div className="row">
                          <div className="col">
                            <span
                              style={{
                                textDecoration: "line-through",
                                fontSize: 14,
                                color: "#878787",
                              }}
                            >
                              {item.prevPrice}
                            </span>{" "}
                            &nbsp;
                            <span
                              style={{
                                color: "#388e3c",
                                fontSize: 13,
                                fontWeight: 500,
                              }}
                            >
                              {item.discount}%
                            </span>
                          </div>
                        </div>
                        <div className="row" style={{ fontSize: 14 }}>
                          <div className="col">{item.EMI}</div>
                        </div>
                        <div className="row" style={{ fontSize: 14 }}>
                          <div className="col">{item.exchange}</div>
                        </div>
                      </div>
                    </div>
                    <hr />
                  </div>
                ))
              : ""}
            <div className="row">
              <div className="col-6">
                <span
                  style={{
                    top: 5,
                    left: 5,
                    position: "relative",
                    fontWeight: "lighter",
                    fontSize: 17,
                  }}
                >
                  Page {pageNumber} of {numberOfPages}
                </span>
              </div>
              <div className="col-6">
                <div className="row">
                  <div>
                    <Pagination
                      itemsCount={totalItemCount}
                      pageSize={numOfItems}
                      currentPage={pageNumber}
                      onPageChange={this.handleChangePage}
                    />
                  </div>
                  {pageNumber < numberOfPages ? (
                    <div>
                      <span
                        className="text-primary"
                        style={{
                          top: 5,
                          left: 5,
                          position: "relative",
                          fontWeight: "lighter",
                          fontSize: 17,
                          cursor: "pointer",
                        }}
                        onClick={() => this.handleNextPage(pageNumber)}
                      >
                        Next
                      </span>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    ) : (
      ""
    );
  }
}

export default ProductDisplay;
