import React, { Component } from "react";
import { getCurrentUser } from "../../../services/flipkartService";
import AddMultipleProduct from "./addMultipleProduct";
import AddSingleProduct from "./addSingleProduct";
import "./dashboard.css";
import UsageReport from "./usageReport/variousReport";
import { Link } from "react-router-dom";
import * as scroll from "react-scroll";
import EditProduct from "./editProduct";

class Admindashboard extends Component {
  state = {
    file: "",
    fileName: "Choose File",
    menus: ["Add Product", "Usage Report"],
    user: null,
    selOpt: { multiProduct: false, singleProduct: false, userActivity: false },
  };
  style = {};

  async componentDidMount() {
    try {
      let res = await getCurrentUser("");
      if (res) {
        this.setState({ user: res.data });
      }
    } catch (error) {}
  }

  handleOption = (opt) => {
    let selOpt = this.state.selOpt;
    selOpt[opt] = !selOpt[opt];
    this.setState({ selOpt });
  };

  render() {
    const { menus, user, selOpt } = this.state;

    if (user && user.role === "admin") {
      return (
        <div className="container">
          <div className="row border py-3 bg-light">
            <div className="col">
              <span className="nav navbar-brand">Admin Console</span>
            </div>
            <div className="col-2"></div>
            <div className="col">
              <div className="row">
                <div
                  className="col-8 mt-2 text-right"
                  style={{ fontSize: "clamp(12px,2vw,18px)" }}
                >
                  <span>Welcome Admin {user.name}</span>
                </div>
                <div className="col mt-2 text-right pl-1">
                  <span style={{ fontSize: "clamp(12px,2vw,18px)" }}>
                    <Link to="/logout">Logout</Link>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className="row justify-content-between bg-primary">
            {menus.map((menu) => (
              <div className="col-5 text-center border py-3 h3 heading">
                <scroll.Link to={menu} smooth={true} duration={2000}>
                  {menu}
                </scroll.Link>
              </div>
            ))}
          </div>
          <div className="row border text-left bg-light mt-3">
            <div
              className="col-6 py-2"
              style={{
                color: "rgb(250, 187, 15)",
              }}
            >
              <scroll.Link to="addMultiplProduct">
                <h4 style={{ fontSize: "clamp(15px,2vw,20px)" }}>
                  Add Multiple Product
                </h4>
              </scroll.Link>
            </div>
            <div className="col-6 text-right text-muted">
              {selOpt.multiProduct ? (
                <i
                  className="fa fa-arrow-down mt-3"
                  style={{
                    color: "rgb(250, 187, 15)",
                    fontSize: "clamp(12px,2vw,20px)",
                  }}
                  onClick={() => this.handleOption("multiProduct")}
                ></i>
              ) : (
                <i
                  className="fa fa-arrow-up mt-3"
                  style={{
                    color: "rgb(250, 187, 15)",
                    fontSize: "clamp(12px,2vw,20px)",
                  }}
                  onClick={() => this.handleOption("multiProduct")}
                ></i>
              )}
            </div>
          </div>
          {selOpt.multiProduct ? (
            <div className="row mb-3 mt-3" id="Add Product">
              <div className="col-12">
                <AddMultipleProduct />
              </div>
            </div>
          ) : (
            ""
          )}

          <div className="row border text-left ">
            <div className="col-6 py-2 bg-light">
              <h4
                style={{
                  color: "rgb(250, 187, 15)",
                  fontSize: "clamp(15px,2vw,20px)",
                }}
              >
                Add Single Product
              </h4>
            </div>
            <div className="col-6 text-right text-muted bg-light">
              {selOpt.singleProduct ? (
                <i
                  className="fa fa-arrow-down mt-3"
                  style={{
                    color: "rgb(250, 187, 15)",
                    fontSize: "clamp(12px,2vw,20px)",
                  }}
                  onClick={() => this.handleOption("singleProduct")}
                ></i>
              ) : (
                <i
                  className="fa fa-arrow-up mt-3"
                  style={{
                    color: "rgb(250, 187, 15)",
                    fontSize: "clamp(12px,2vw,20px)",
                  }}
                  onClick={() => this.handleOption("singleProduct")}
                ></i>
              )}
            </div>
          </div>
          {selOpt.singleProduct ? (
            <div className="row">
              <div className="col-12">
                <AddSingleProduct />
              </div>
            </div>
          ) : (
            ""
          )}

          <div className="row border text-left bg-light">
            <div
              className="col-6 py-2"
              style={{
                color: "rgb(250, 187, 15)",

                textUnderlineOffset: "1em",
              }}
            >
              <h4 style={{ fontSize: "clamp(15px,2vw,20px)" }}>Reports</h4>
            </div>
            <div className="col-6 text-right text-muted">
              {selOpt.userActivity ? (
                <i
                  className="fa fa-arrow-down mt-3"
                  style={{
                    color: "rgb(250, 187, 15)",
                    fontSize: "clamp(12px,2vw,20px)",
                  }}
                  onClick={() => this.handleOption("userActivity")}
                ></i>
              ) : (
                <i
                  className="fa fa-arrow-up mt-3"
                  style={{
                    color: "rgb(250, 187, 15)",
                    fontSize: "clamp(12px,2vw,20px)",
                  }}
                  onClick={() => this.handleOption("userActivity")}
                ></i>
              )}
            </div>
          </div>
          {selOpt.userActivity ? (
            <div className="row border mt-2 pt-2">
              <div className="col-12">
                <UsageReport onEdit={this.handleEdit} />
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
      );
    } else {
      return (
        <div className="row ">
          <div className="col-12">
            You are not authorize to view this page.Please Login as Admin.
            <Link to="/login">Login</Link>
          </div>
        </div>
      );
    }
  }
}

export default Admindashboard;
