import React, { Component } from "react";
import FlipkartNavbar from "./header";
import * as flipkartService from "../../services/flipkartService";
import { connect } from "react-redux";
import queryString from "query-string";
import { postCart, postUrl } from "../common/apiCall";
import { getCurrentUser } from "./../../services/flipkartService";

class SelectedProduct extends Component {
  state = {
    product: {},
    pics: {},
    images: [],
    selectedImage: "",
    details: [],
    bankOffers: [],
  };
  async componentDidMount() {
    try {
      let { category, id } = this.props.match.params;
      let params = this.props.location.search;
      let url = this.props.match.url + params;
      postUrl(url);
      const response = await flipkartService.getProductById(category, id);
      const myOffers = await flipkartService.getAllBankOffers();

      const images = JSON.parse(JSON.stringify(response.data.pics.imgList));
      const details = JSON.parse(JSON.stringify(response.data.prod.details));
      this.setState({
        product: { ...response.data.prod },
        pics: response.data.pics,
        images: images,
        details: details,
        selectedImage: images[0],
        bankOffers: myOffers.data,
      });
    } catch (error) {}
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamName) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  handleSelectedImg = (img) => {
    this.setState({ selectedImage: img });
  };

  handleCart = () => {
    let item = { ...this.state.product };
    let token = JSON.parse(sessionStorage.getItem("token"))
      ? JSON.parse(sessionStorage.getItem("token"))
      : "NA";

    getCurrentUser(token)
      .then((res) => {
        if (res.data.email) {
          let data = {
            id: item.id,
            category: item.category,
            name: item.name,
            img: item.img,
          };
          flipkartService
            .postCartReport(data)
            .then((res) => {
              console.log("success");
              this.props.dispatch({ type: "ADD_CART", item: item, update: 1 });
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })
      .catch((err) => {
        console.log(err);
        alert("Please Login First");
      });
  };

  handleBuy = async () => {
    try {
      let user = await flipkartService.getCurrentUser();
      if (user !== null) {
        let item = { ...this.state.product };

        let data = {
          id: item.id,
          category: item.category,
          name: item.name,
          img: item.img,
        };
        try {
          let response = await flipkartService.postCartReport(data);
          if (response) {
            this.props.dispatch({ type: "ADD_CART", item: item, update: 1 });
            this.props.history.push("/home/checkout");
          }
        } catch (error) {}
      }
    } catch (error) {
      alert("Please Login First");
    }
  };
  handleSearch = async (search) => {
    let params = "";
    params = this.addToParams(params, "q", search);
    params = this.addToParams(params, "page", 1);

    let data = {
      keyword: search,
    };
    try {
      let response = await flipkartService.postSearchReport(data);
    } catch (error) {}
    this.props.history.push(`/home/products${params}`);
  };

  render() {
    const {
      product,
      pics,
      images,
      selectedImage,
      details,
      bankOffers,
    } = this.state;

    return (
      <div className="container-fluid">
        <FlipkartNavbar
          search={this.state.search}
          onSubmit={this.handleSearch}
        />
        <br />
        <div className="row">
          <div className="col-lg-5 col-12">
            <div className="row p-0">
              <div className="col-lg-2 col-4 text-center">
                {images.map((img, index) =>
                  selectedImage === img ? (
                    <div
                      style={{
                        height: 64,
                        width: 64,

                        borderWidth: "2px solid",
                      }}
                      key={index}
                      className="row border ml-lg-2 ml-0 border-primary"
                    >
                      <div
                        className="col text-center"
                        onClick={() => this.handleSelectedImg(img)}
                      >
                        <img style={{ width: 40, height: 50 }} src={img} />
                      </div>
                    </div>
                  ) : (
                    <div
                      style={{
                        height: 64,
                        width: 64,
                        textAlign: "center",
                        borderWidth: "2px solid",
                      }}
                      key={index}
                      className="row border ml-lg-2 ml-0 border-light"
                    >
                      <div
                        className="col text-center"
                        onClick={() => this.handleSelectedImg(img)}
                      >
                        <img style={{ width: 40, height: 50 }} src={img} />
                      </div>
                    </div>
                  )
                )}
              </div>

              <div className="col-lg-8 col-8 border p-0 text-center">
                <img className="img-fluid" src={selectedImage} />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-2 col-4"></div>
              <div className="col-lg-4 col-4 text-center ">
                <button
                  className="btn btn-sm btn-warning text-white"
                  onClick={() => this.handleCart()}
                >
                  <svg
                    className="_3oJBMI"
                    height="16"
                    viewBox="0 0 16 15"
                    width="16"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      className=""
                      d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                      fill="#fff"
                    ></path>
                  </svg>
                  Add to Cart
                </button>
              </div>
              <div className="col-lg-4 col-4  text-center">
                <button
                  className="btn btn-sm text-white"
                  style={{ backgroundColor: "#fb641b" }}
                  onClick={() => this.handleBuy()}
                >
                  <i className="fa fa-bolt"></i> Buy Now
                </button>
              </div>
            </div>
          </div>
          <div className="col-lg-7 col-12">
            <div
              className="row"
              style={{
                color: "#212121",
                fontSize: 18,
                fontWeight: 400,
                padding: 0,
                lineHeight: 1.4,
                fontSize: "inherit",
                fontWeight: "inherit",
                display: "inline-block",
              }}
            >
              <div className="col"> {product.name} </div>
            </div>
            <div className="row">
              <div className="col">
                <span
                  style={{
                    lineHeight: "normal",
                    display: "inline-block",
                    color: "#fff",
                    padding: "2px 4px 2px 6px",
                    borderRadius: "3px",
                    fontWeight: 500,
                    fontSize: 12,
                    verticalAlign: "middle",
                    backgroundColor: "#388e3c",
                  }}
                >
                  <strong>
                    4.5&nbsp;
                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" />
                  </strong>
                </span>{" "}
                &nbsp;
                <span
                  className="text-muted"
                  style={{ fontSize: 14, fontWeight: 500 }}
                >
                  {product.ratingDesc}
                </span>
                &nbsp;&nbsp;
                <span>
                  <img
                    className="img-fluid"
                    src="https://i.ibb.co/XCQBqSr/fa-8b4b59.png"
                    style={{ width: 70 }}
                  />
                </span>
              </div>
            </div>
            <div className="row mt-2">
              <div
                className="col"
                style={{ fontSize: 28, verticalAlign: "sub", fontWeight: 500 }}
              >
                {product.price} &nbsp;
                <span
                  style={{
                    textDecoration: "line-through",
                    fontSize: 16,
                    color: "#878787",
                  }}
                >
                  {product.prevPrice}
                </span>
                &nbsp;
                <span
                  style={{ color: "#388e3c", fontSize: 16, fontWeight: 500 }}
                >
                  {product.discount}%
                </span>
              </div>
            </div>
            <div className="row">
              <div
                className="col"
                style={{
                  fontSize: 16,
                  marginLeft: 12,
                  verticalAlign: "middle",
                  fontWeight: 500,
                  color: "black",
                }}
              >
                Available Offers
              </div>
            </div>
            <div className="row">
              {bankOffers.map((offer, index) => (
                <div className="col-12" key={index}>
                  <img style={{ height: 18, width: 18 }} src={offer.img} />
                  &nbsp;
                  <span
                    style={{
                      color: "#212121",
                      fontWeight: 500,
                      paddingRight: 4,
                      fontSize: 14,
                    }}
                  >
                    {offer.type}
                  </span>
                  &nbsp;
                  <span style={{ fontSize: 14 }}>{offer.detail}</span>
                </div>
              ))}
            </div>
            <div className="row mt-2">
              <div className="col-lg-1 col-3 border text-center ml-2">
                <img style={{ width: 38, height: 30 }} src={pics.brandImg} />
              </div>
              <div
                className="col-8 ml-3 d-none d-lg-block"
                style={{ fontSize: 14 }}
              >
                Brand Warranty of 1 Year Available for Mobile and 6 Months for
                Accessories
              </div>
            </div>
            <div className="row mt-2">
              <div
                className="col-1 d-none d-lg-block"
                style={{
                  fontWeight: 500,
                  color: "#878787",
                  width: 110,
                  paddingRight: 10,
                  float: "left",
                  fontSize: 14,
                }}
              >
                Highlights
              </div>
              <div
                className="col-5 d-none d-lg-block"
                style={{ fontWeight: 500 }}
              >
                <ul>
                  {details.map((detail, index) => (
                    <li key={index} style={{ fontSize: 14 }}>
                      {detail}
                    </li>
                  ))}
                </ul>
              </div>
              <div
                className="col-2 d-none d-lg-block"
                style={{
                  fontWeight: 500,
                  color: "#878787",
                  width: 110,
                  paddingRight: 10,
                  float: "left",
                  fontSize: 14,
                }}
              >
                Easy Payment Options
              </div>
              <div className="col-4 d-none d-lg-block">
                <ul style={{ fontSize: 14, color: "grey", lineHeight: 1.4 }}>
                  <li>
                    <span style={{ color: "black" }}>{product.emi}</span>
                  </li>
                  <li>
                    <span style={{ color: "black" }}>
                      Debit/Flipkart EMI available
                    </span>
                  </li>
                  <li>
                    <span style={{ color: "black" }}>Cash on Delivery</span>
                  </li>
                  <li>
                    <span style={{ color: "black" }}>
                      Net Banking &amp; Credit/Debit/ATM Card
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="row">
              <div
                className="col-lg-1 col-3"
                style={{
                  fontWeight: 500,
                  color: "#878787",
                  width: 110,
                  paddingRight: 10,
                  float: "left",
                  fontSize: 14,
                }}
              >
                Seller
              </div>
              <div className="col-lg-9 col-9">
                <span
                  style={{
                    color: "#2874f0",
                    fontSize: "16px",
                    fontWeight: 500,
                  }}
                >
                  SuperComNet&nbsp;&nbsp;
                </span>
                <span
                  style={{
                    lineHeight: "normal",
                    display: "inline-block",
                    color: "#fff",
                    padding: "2px 4px 2px 6px",
                    fontWeight: 500,
                    fontSize: 12,
                    borderRadius: 4,
                    backgroundColor: "#2874f0",
                  }}
                >
                  4.2 &nbsp;
                  <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" />
                </span>
                <ul className="d-none d-lg-block">
                  <li style={{ fontSize: 14, color: "grey", lineHeight: 1.4 }}>
                    10 Day replacement
                  </li>
                </ul>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <img
                  className="img-fluid"
                  src="https://i.ibb.co/j8CMRbn/CCO-PP-2019-07-14.png"
                  style={{ width: 300, height: 85 }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(SelectedProduct);
